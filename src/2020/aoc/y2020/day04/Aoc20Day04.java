package aoc.y2020.day04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2020.Aoc20Day;

public class Aoc20Day04 extends Aoc20Day {

    private static final Pattern HGT_PATTERN = Pattern.compile("^([0-9]{2,3})(cm|in)$");

    private static final Pattern HCL_PATTERN = Pattern.compile("^#[0-9a-f]{6}$");

    private static final List<String> ECL_VALUES = Arrays.asList("amb", "blu", "brn", "gry", "grn", "hzl", "oth");

    private static final Pattern PID_PATTERN = Pattern.compile("^[0-9]{9}$");

    private enum PassportKey {
        byr, iyr, eyr, hgt, hcl, ecl, pid, cid;

        static PassportKey getByName(String name) {
            for (PassportKey value : values()) {
                if (value.name().equals(name)) {
                    return value;
                }
            }

            return null;
        }
    }

    private List<EnumMap<PassportKey, String>> buildPassportList(String resPath) {
        List<String> lines = readStringList(resPath, "\\s+", true, true);
        List<EnumMap<PassportKey, String>> passportList = new ArrayList<>();
        EnumMap<PassportKey, String> passport = new EnumMap<>(PassportKey.class);

        for (String line : lines) {
            if ((line.length() > 4) && (line.charAt(3) == ':')) {
                PassportKey key = PassportKey.getByName(line.substring(0, 3));

                if (key != null) {
                    passport.put(key, line.substring(4));
                }
            } else if (line.isEmpty() && !passport.isEmpty()) {
                passportList.add(passport);
                passport = new EnumMap<>(PassportKey.class);
            }
        }

        if (!passport.isEmpty()) {
            passportList.add(passport);
        }

        return passportList;
    }

    private static boolean validate1(EnumMap<PassportKey, String> passport) {
        for (PassportKey key : PassportKey.values()) {
            if ((key != PassportKey.cid) && !passport.containsKey(key)) {
                return false;
            }
        }

        return true;
    }

    private static boolean validate2(EnumMap<PassportKey, String> passport) {
        if (!validate1(passport)) {
            return false;
        }

        for (Entry<PassportKey, String> entry : passport.entrySet()) {
            String value = entry.getValue();

            try {
                switch (entry.getKey()) {
                case byr:
                    try {
                        int bYear = Integer.parseInt(value);

                        if ((bYear < 1920) || (bYear > 2002)) {
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        return false;
                    }

                    break;
                case iyr:
                    try {
                        int iYear = Integer.parseInt(value);

                        if ((iYear < 2010) || (iYear > 2020)) {
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        return false;
                    }

                    break;
                case eyr:
                    try {
                        int eYear = Integer.parseInt(value);

                        if ((eYear < 2020) || (eYear > 2030)) {
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        return false;
                    }

                    break;
                case hgt:
                    Matcher hgtMatcher = HGT_PATTERN.matcher(value);

                    if (!hgtMatcher.matches()) {
                        return false;
                    }

                    try {
                        int size = Integer.parseInt(hgtMatcher.group(1));
                        boolean metric = "cm".equals(hgtMatcher.group(2));

                        if (metric ? ((size < 150) || (size > 193)) : ((size < 59) || (size > 76))) {
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        return false;
                    }

                    break;
                case hcl:
                    if (!HCL_PATTERN.matcher(value).matches()) {
                        return false;
                    }

                    break;
                case ecl:
                    if (!ECL_VALUES.contains(value)) {
                        return false;
                    }

                    break;
                case pid:
                    if (!PID_PATTERN.matcher(value).matches()) {
                        return false;
                    }

                    break;
                default:
                }
            } catch (NumberFormatException e) {
                return false;
            }
        }

        return true;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        int result = 0;
        List<EnumMap<PassportKey, String>> passportList = buildPassportList(resPath);

        for (EnumMap<PassportKey, String> passport : passportList) {
            if (validate1(passport)) {
                result++;
            }
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int result = 0;
        List<EnumMap<PassportKey, String>> passportList = buildPassportList(resPath);

        for (EnumMap<PassportKey, String> passport : passportList) {
            if (validate2(passport)) {
                result++;
            }
        }

        return result;
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample");
        executePuzzle(2, "SampleInvalid");
        executePuzzle(2, "SampleValid");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc20Day04().execute(args);
    }
}
