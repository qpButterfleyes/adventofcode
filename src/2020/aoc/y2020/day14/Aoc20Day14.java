package aoc.y2020.day14;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import aoc.y2020.Aoc20Day;

public class Aoc20Day14 extends Aoc20Day {

    private static final Pattern LINE_PATTERN = Pattern.compile("mask = ([01X]+)|mem\\[(\\d+)\\] = (\\d+)");

    private static class ScriptLine {
        final long orMask;
        final long andMask;
        final long floatMask;
        final long[] floatingBitValues;
        final long address;
        final long value;

        ScriptLine(long oMask, long aMask, long fMask, long[] fValues, long addr, long val) {
            orMask = oMask;
            andMask = aMask;
            floatMask = fMask;
            floatingBitValues = fValues;
            address = addr;
            value = val;
        }
    }

    public List<ScriptLine> buildScript(String resPath) {
        List<ScriptLine> script = new ArrayList<>();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);
        long orMask = 0;
        long andMask = -1;
        long floatMask = 0;
        long[] floatingBitValues = new long[0];

        for (Matcher matcher : matcherList) {
            try {
                if (matcher.group(1) != null) {
                    String mask = matcher.group(1);
                    orMask = Long.parseLong(mask.replace('X', '0'), 2);
                    andMask = Long.parseLong(mask.replace('X', '1'), 2);
                    floatMask = Long.parseLong(mask.replace('1', '0').replace('X', '1'), 2);
                    floatingBitValues = IntStream.range(0, mask.length()).filter(i -> mask.charAt(i) == 'X')
                            .mapToLong(i -> 1L << (mask.length() - 1 - i)).toArray();
                } else {
                    long address = Long.parseLong(matcher.group(2));
                    long value = Long.parseLong(matcher.group(3));
                    script.add(new ScriptLine(orMask, andMask, floatMask, floatingBitValues, address, value));
                }
            } catch (NumberFormatException e) {
                logError("Parsing integers in '" + matcher.group() + "' failed", e);
            }
        }

        return script;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        Map<Long, Long> memory = new HashMap<>();
        List<ScriptLine> script = buildScript(resPath);

        for (ScriptLine line : script) {
            memory.put(line.address, (line.value | line.orMask) & line.andMask);
        }

        return memory.values().stream().mapToLong(Long::longValue).sum();
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        Map<Long, Long> memory = new HashMap<>();
        List<ScriptLine> script = buildScript(resPath);

        for (ScriptLine line : script) {
            long minAddress = (line.address | line.orMask) & (-1 ^ line.floatMask);
            long[] floatingBitValues = line.floatingBitValues;
            int floatingBitCount = floatingBitValues.length;

            for (int i = 0; i < (1 << floatingBitCount); i++) {
                int offsetIndex = i;
                long offset = IntStream.range(0, floatingBitCount)
                        .mapToLong(j -> ((offsetIndex >> j) % 2) * floatingBitValues[j]).sum();
                memory.put(minAddress | offset, line.value);
            }
        }

        return memory.values().stream().mapToLong(Long::longValue).sum();
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample1");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample2");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc20Day14().execute(args);
    }
}
