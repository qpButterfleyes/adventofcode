package aoc.y2020.day10;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import aoc.y2020.Aoc20Day;

public class Aoc20Day10 extends Aoc20Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        int[] numbers = readStringList(resPath).stream().mapToInt(Integer::parseInt).toArray();
        Arrays.sort(numbers);
        int step1count = 0;
        int step3count = 0;
        int last = 0;

        for (int i = 0; i < numbers.length; i++) {
            int diff = numbers[i] - last;
            last = numbers[i];

            switch (diff) {
            case 1:
                step1count++;
                break;
            case 3:
                step3count++;
                break;
            default:
            }
        }

        // Add the step to the built-in adapter (always 3).
        step3count++;
        return step1count * step3count;
    }

    private long countPaths(int current, Integer target, Set<Integer> numbers, Map<Integer, Long> pathCountMap) {
        long result = 0;

        for (int i = 1; i <= 3; i++) {
            int next = current + i;

            if (next == target) {
                return result + 1;
            }

            Long pathCount = pathCountMap.get(next);

            if (pathCount != null) {
                result += pathCount;
            } else if (numbers.contains(next)) {
                pathCount = countPaths(next, target, numbers, pathCountMap);
                pathCountMap.put(next, pathCount);
                result += pathCount;
            }
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        Set<Integer> numbers = readStringList(resPath).stream().map(Integer::valueOf).collect(Collectors.toSet());
        return countPaths(0, Collections.max(numbers), numbers, new HashMap<>());
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample1");
        executePuzzle(1, "Sample2");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample1");
        executePuzzle(2, "Sample2");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc20Day10().execute(args);
    }
}
