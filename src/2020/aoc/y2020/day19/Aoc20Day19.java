package aoc.y2020.day19;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import aoc.y2020.Aoc20Day;

public class Aoc20Day19 extends Aoc20Day {

    private static final Pattern RULE_PATTERN = Pattern
            .compile("(\\d+): ((\"(a|b)\")|(\\d+)( (\\d+)( (\\d+))?)?( \\| (\\d+)( (\\d+)( (\\d+))?)?)?)");

    private class Rule {

        final List<List<Integer>> choiceIDLists = new ArrayList<>();

        final List<List<Rule>> choices = new ArrayList<>();

        String literal = null;

        void link() {
            for (List<Integer> idList : choiceIDLists) {
                List<Rule> ruleList = idList.stream().map(i -> ruleMap.get(i)).collect(Collectors.toList());
                choices.add(ruleList);
            }
        }

        Set<Integer> match(String message, Set<Integer> positions) {
            Set<Integer> result = new HashSet<>();

            for (int pos : positions) {
                if (pos >= message.length()) {
                    continue;
                }

                if (literal != null) {
                    if (message.substring(pos).startsWith(literal)) {
                        result.add(pos + 1);
                    }

                    continue;
                }

                for (List<Rule> choice : choices) {
                    Set<Integer> tmpPositions = new HashSet<>();
                    tmpPositions.add(pos);

                    for (Rule rule : choice) {
                        tmpPositions = rule.match(message, tmpPositions);
                    }

                    result.addAll(tmpPositions);
                }
            }

            return result;
        }
    }

    private List<String> messageList;

    private List<String> ruleStringList;

    private Map<Integer, Rule> ruleMap;

    private void readRulesAndMessages(String resPath) {
        List<List<String>> stringLists = readStringGroupList(resPath, null);
        messageList = stringLists.get(1);
        ruleStringList = stringLists.get(0);
    }

    private void buildRuleMap() {
        ruleMap = new HashMap<>();
        List<Matcher> ruleMatcherList = buildMatcherList(ruleStringList, RULE_PATTERN);

        for (Matcher matcher : ruleMatcherList) {
            Rule rule = new Rule();

            if (matcher.group(4) != null) {
                rule.literal = matcher.group(4);
            } else if (matcher.group(5) != null) {
                List<Integer> choice1 = new ArrayList<>();
                choice1.add(Integer.parseInt(matcher.group(5)));

                if (matcher.group(7) != null) {
                    choice1.add(Integer.parseInt(matcher.group(7)));

                    if (matcher.group(9) != null) {
                        choice1.add(Integer.parseInt(matcher.group(9)));
                    }
                }

                rule.choiceIDLists.add(choice1);

                if (matcher.group(11) != null) {
                    List<Integer> choice2 = new ArrayList<>();
                    choice2.add(Integer.parseInt(matcher.group(11)));

                    if (matcher.group(13) != null) {
                        choice2.add(Integer.parseInt(matcher.group(13)));

                        if (matcher.group(15) != null) {
                            choice2.add(Integer.parseInt(matcher.group(15)));
                        }
                    }

                    rule.choiceIDLists.add(choice2);
                }
            }

            ruleMap.put(Integer.valueOf(matcher.group(1)), rule);
        }

        for (Rule rule : ruleMap.values()) {
            rule.link();
        }
    }

    private void filterMessages() {
        Iterator<String> it = messageList.iterator();

        while (it.hasNext()) {
            String message = it.next();
            Set<Integer> initPos = new HashSet<>();
            initPos.add(0);

            if (!ruleMap.get(0).match(message, initPos).contains(message.length())) {
                it.remove();
            }
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        readRulesAndMessages(resPath);
        buildRuleMap();
        filterMessages();
        return messageList.size();
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        readRulesAndMessages(resPath);
        int indexOfRule8 = ruleStringList.indexOf("8: 42");
        int indexOfRule11 = ruleStringList.indexOf("11: 42 31");

        if (indexOfRule8 >= 0) {
            ruleStringList.set(indexOfRule8, "8: 42 | 42 8");
        }

        if (indexOfRule8 >= 0) {
            ruleStringList.set(indexOfRule11, "11: 42 31 | 42 11 31");
        }

        buildRuleMap();
        filterMessages();
        return messageList.size();
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample1");
        executePuzzle(1, "Sample2");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample2");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc20Day19().execute(args);
    }
}
