package aoc.y2020.day25;

import aoc.y2020.Aoc20Day;

public class Aoc20Day25 extends Aoc20Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        long[] publicKeys = readStringList(resPath).stream().mapToLong(Long::parseLong).toArray();
        int[] iterations = new int[2];
        long[] encryptionKey = new long[2];
        long subjectNumber = 7;
        long value;

        for (int i = 0; i < 2; i++) {
            value = 1;

            while (value != publicKeys[i]) {
                iterations[i]++;
                value = (value * subjectNumber) % 20201227;
            }
        }

        for (int i = 0; i < 2; i++) {
            subjectNumber = publicKeys[i];
            value = 1;

            for (int j = 0; j < iterations[1 - i]; j++) {
                value = (value * subjectNumber) % 20201227;
            }

            encryptionKey[i] = value;
        }

        return encryptionKey[0];
    }

    @Override
    public String getPuzzle2String(String resPath) {
        // Nothing to do.
        return "";
    }

    public static void main(String[] args) {
        new Aoc20Day25().execute(args);
    }
}
