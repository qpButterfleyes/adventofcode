package aoc.y2020.day09;

import java.util.List;

import aoc.y2020.Aoc20Day;

public class Aoc20Day09 extends Aoc20Day {

    private boolean findPair(long[] numbers, int preambleLength, int index) {
        long sum = numbers[index];

        for (int i = index - preambleLength; i < index; i++) {
            for (int j = i + 1; j < index; j++) {
                if ((numbers[i] + numbers[j]) == sum) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        int preambleLength = Integer.parseInt(stringList.remove(0));
        long[] numbers = stringList.stream().mapToLong(Long::parseLong).toArray();

        for (int i = preambleLength; i < numbers.length; i++) {
            if (!findPair(numbers, preambleLength, i)) {
                return numbers[i];
            }
        }

        return 0;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        stringList.remove(0);
        long[] numbers = stringList.stream().mapToLong(Long::parseLong).toArray();
        long sum = getPuzzle1Long(resPath);

        for (int i = 0; i < numbers.length; i++) {
            long tmpSum = numbers[i];
            long min = numbers[i];
            long max = numbers[i];

            for (int j = i + 1; j < numbers.length; j++) {
                tmpSum += numbers[j];

                if (tmpSum > sum) {
                    break;
                }

                if (numbers[j] < min) {
                    min = numbers[j];
                } else if (numbers[j] > max) {
                    max = numbers[j];
                }

                if (tmpSum == sum) {
                    return min + max;
                }
            }
        }

        return 0;
    }

    public static void main(String[] args) {
        new Aoc20Day09().execute(args);
    }
}
