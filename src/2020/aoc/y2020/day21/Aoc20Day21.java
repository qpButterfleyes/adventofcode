package aoc.y2020.day21;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2020.Aoc20Day;

public class Aoc20Day21 extends Aoc20Day {

    private static final Pattern INGREDIENT_PATTERN = Pattern.compile("([a-z]+ )");

    private static final Pattern ALLERGEN_PATTERN = Pattern.compile("( [a-z]+)");

    private static final Pattern LINE_PATTERN = Pattern
            .compile("(" + INGREDIENT_PATTERN.pattern() + "+)\\(contains((" + ALLERGEN_PATTERN.pattern() + ",?)+)\\)");

    private static class FoodInfo {
        final List<String> ingredientList = new ArrayList<>();
        final List<String> allergenList = new ArrayList<>();
    }

    private List<FoodInfo> foodInfoList;

    private Set<String> safeIngredientSet;

    private Map<String, String> sourceMap;

    public void buildFoodInfoList(String resPath) {
        foodInfoList = new ArrayList<>();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            FoodInfo foodInfo = new FoodInfo();
            foodInfoList.add(foodInfo);
            String ingredientString = matcher.group(1);
            String allergenString = matcher.group(3);
            Matcher ingredientMatcher = INGREDIENT_PATTERN.matcher(ingredientString);
            Matcher allergenMatcher = ALLERGEN_PATTERN.matcher(allergenString);

            while (ingredientMatcher.find()) {
                foodInfo.ingredientList.add(ingredientMatcher.group(1).trim());
            }

            while (allergenMatcher.find()) {
                foodInfo.allergenList.add(allergenMatcher.group(1).trim());
            }
        }
    }

    private void mapAllergenSources() {
        safeIngredientSet = new HashSet<>();
        sourceMap = new TreeMap<>();
        Map<String, List<String>> possibleSourceMap = new HashMap<>();

        for (FoodInfo foodInfo : foodInfoList) {
            List<String> ingredientList = foodInfo.ingredientList;
            safeIngredientSet.addAll(ingredientList);

            for (String allergen : foodInfo.allergenList) {
                if (possibleSourceMap.containsKey(allergen)) {
                    possibleSourceMap.get(allergen).retainAll(ingredientList);
                } else {
                    possibleSourceMap.put(allergen, new ArrayList<>(ingredientList));
                }
            }
        }

        while (!possibleSourceMap.isEmpty()) {
            Iterator<Entry<String, List<String>>> it = possibleSourceMap.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, List<String>> entry = it.next();
                String allergen = entry.getKey();
                List<String> ingredientList = entry.getValue();

                if (ingredientList.size() == 1) {
                    it.remove();
                    String sourceIngredient = ingredientList.get(0);
                    sourceMap.put(allergen, sourceIngredient);
                    safeIngredientSet.remove(sourceIngredient);

                    for (List<String> otherIngredientList : possibleSourceMap.values()) {
                        otherIngredientList.remove(sourceIngredient);
                    }
                }
            }
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        long result = 0;
        buildFoodInfoList(resPath);
        mapAllergenSources();

        for (FoodInfo foodInfo : foodInfoList) {
            result += foodInfo.ingredientList.stream().filter(i -> safeIngredientSet.contains(i)).count();
        }

        return result;
    }

    @Override
    public String getPuzzle2String(String resPath) {
        buildFoodInfoList(resPath);
        mapAllergenSources();
        StringBuilder sb = new StringBuilder();
        sourceMap.entrySet().stream().map(Entry<String, String>::getValue).forEach(s -> sb.append(s).append(','));
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

    public static void main(String[] args) {
        new Aoc20Day21().execute(args);
    }
}
