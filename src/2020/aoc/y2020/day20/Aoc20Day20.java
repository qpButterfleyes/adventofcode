package aoc.y2020.day20;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import aoc.y2020.Aoc20Day;

public class Aoc20Day20 extends Aoc20Day {

    private static final int TILE_SIZE = 10;

    private static final int TILE_MAX_IDX = TILE_SIZE - 1;

    private static class Edge {

        final boolean[] data = new boolean[TILE_SIZE];

        Edge match = null;

        final Tile tile;

        Edge(Tile tile) {
            this.tile = tile;
        }

        private void swap(int i, int j) {
            boolean tmpValue = data[i];
            data[i] = data[j];
            data[j] = tmpValue;
        }

        void flip() {
            IntStream.range(0, TILE_SIZE / 2).forEach(i -> swap(i, TILE_MAX_IDX - i));
        }

        boolean mirrors(Edge other) {
            for (int i = 0; i < TILE_SIZE; i++) {
                if (other.data[i] != data[TILE_MAX_IDX - i]) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }

            if (!(obj instanceof Edge)) {
                return false;
            }

            Edge other = (Edge) obj;
            return Arrays.equals(data, other.data);
        }
    }

    private static class Tile {

        long id = 0;

        final boolean[][] data = new boolean[TILE_SIZE][TILE_SIZE];

        final Edge[] edges = new Edge[] { new Edge(this), new Edge(this), new Edge(this), new Edge(this) };

        void flipHorizontal() {
            Aoc20Day20.flipHorizontal(data);
            Edge tmpEdge = edges[3];
            edges[3] = edges[1];
            edges[1] = tmpEdge;
            IntStream.range(0, 4).forEach(i -> edges[i].flip());
        }

        void rotateRight() {
            Aoc20Day20.rotateRight(data);
            Edge tmpEdge = edges[3];
            edges[3] = edges[2];
            edges[2] = edges[1];
            edges[1] = edges[0];
            edges[0] = tmpEdge;
        }
    }

    private List<Tile> tileList;

    private int tileGridSize;

    private int tileGridMaxIdx;

    private Tile[][] tileGrid;

    private int imageSize;

    private boolean[][] image;

    private boolean[][] seaMonster;

    private int seaMonsterSizeY;

    private int seaMonsterSizeX;

    static void flipHorizontal(boolean[][] data) {
        int size = data.length;
        boolean[][] newData = new boolean[size][size];

        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                newData[y][x] = data[y][size - 1 - x];
            }
        }

        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                data[y][x] = newData[y][x];
            }
        }
    }

    static void rotateRight(boolean[][] data) {
        int size = data.length;
        boolean[][] newData = new boolean[size][size];

        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                newData[y][x] = data[size - 1 - x][y];
            }
        }

        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                data[y][x] = newData[y][x];
            }
        }
    }

    // Currently not used. Added here for debugging.
    static void print(boolean[][] data) {
        int size = data.length;

        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                System.out.print(data[y][x] ? "#" : ".");
            }

            System.out.println();
        }

        System.out.println();
    }

    private void buildTileList(String resPath) {
        tileList = new ArrayList<>();
        List<List<String>> stringGroupList = readStringGroupList(resPath, null);

        for (List<String> stringGroup : stringGroupList) {
            Tile tile = new Tile();
            tile.id = Long.parseLong(stringGroup.remove(0).substring(5, 9));
            boolean[][] data = tile.data;
            Edge[] edges = tile.edges;

            // y increases top-to-bottom, x increases left-to-right.
            for (int y = 0; y < TILE_SIZE; y++) {
                for (int x = 0; x < TILE_SIZE; x++) {
                    data[y][x] = stringGroup.get(y).charAt(x) == '#';
                }
            }

            // The edges are numbered clockwise top-right-bottom-left.
            // The pixel index in each edge also increases clockwise.
            for (int i = 0; i < TILE_SIZE; i++) {
                edges[0].data[i] = data[0][i];
                edges[1].data[i] = data[i][TILE_MAX_IDX];
                edges[2].data[i] = data[TILE_MAX_IDX][TILE_MAX_IDX - i];
                edges[3].data[i] = data[TILE_MAX_IDX - i][0];
            }

            tileList.add(tile);
        }

        tileGridSize = (int) Math.round(Math.sqrt(tileList.size()));
        tileGridMaxIdx = tileGridSize - 1;
        tileGrid = new Tile[tileGridSize][tileGridSize];
    }

    void linkMatchingEdges() {
        for (Tile tile : tileList) {
            EDGES: for (int i = 0; i < 4; i++) {
                Edge edge = tile.edges[i];

                if (edge.match != null) {
                    continue;
                }

                for (Tile otherTile : tileList) {
                    if (otherTile == tile) {
                        continue;
                    }

                    for (int j = 0; j < 4; j++) {
                        Edge otherEdge = otherTile.edges[j];

                        if (edge.equals(otherEdge) || edge.mirrors(otherEdge)) {
                            edge.match = otherEdge;
                            otherEdge.match = edge;
                            continue EDGES;
                        }
                    }
                }
            }
        }
    }

    void setInitialCornerTile() {
        for (Tile tile : tileList) {
            if (Arrays.stream(tile.edges).filter(e -> e.match == null).count() == 2) {
                // We found a corner tile. Rotate it until the unmatched edges are left and top...
                while ((tile.edges[3].match != null) || (tile.edges[0].match != null)) {
                    tile.rotateRight();
                }

                // ...and put it into the top-left corner of the image.
                tileGrid[0][0] = tile;
                return;
            }
        }
    }

    void addTileColumn(int x) {
        for (int y = 1; y < tileGridSize; y++) {
            Edge baseEdge = tileGrid[y - 1][x].edges[2];
            Edge newEdge = baseEdge.match;
            Tile newTile = newEdge.tile;
            tileGrid[y][x] = newTile;

            if (!newEdge.mirrors(baseEdge)) {
                newTile.flipHorizontal();
            }

            while (newEdge != newTile.edges[0]) {
                newTile.rotateRight();
            }
        }
    }

    void addTileRow(int y) {
        for (int x = 1; x < tileGridSize; x++) {
            Edge baseEdge = tileGrid[y][x - 1].edges[1];
            Edge newEdge = baseEdge.match;
            Tile newTile = newEdge.tile;
            tileGrid[y][x] = newTile;

            if (!newEdge.mirrors(baseEdge)) {
                newTile.flipHorizontal();
            }

            while (newEdge != newTile.edges[3]) {
                newTile.rotateRight();
            }
        }
    }

    private void buildTileGrid(String resPath) {
        buildTileList(resPath);
        linkMatchingEdges();
        setInitialCornerTile();
        addTileColumn(0);

        for (int y = 0; y < tileGridSize; y++) {
            addTileRow(y);
        }
    }

    private void buildImage() {
        int tileImageSize = TILE_SIZE - 2;
        imageSize = tileGridSize * tileImageSize;
        image = new boolean[imageSize][imageSize];

        for (int gridY = 0; gridY < tileGridSize; gridY++) {
            for (int gridX = 0; gridX < tileGridSize; gridX++) {
                for (int tileY = 1; tileY < TILE_MAX_IDX; tileY++) {
                    for (int tileX = 1; tileX < TILE_MAX_IDX; tileX++) {
                        int imageY = ((gridY * tileImageSize) + tileY) - 1;
                        int imageX = ((gridX * tileImageSize) + tileX) - 1;
                        image[imageY][imageX] = tileGrid[gridY][gridX].data[tileY][tileX];
                    }
                }
            }
        }
    }

    private void buildSeaMonster() {
        List<String> stringList = readStringList(getClass().getSimpleName() + "SeaMonster.txt", null, false, false);
        seaMonsterSizeY = stringList.size();
        seaMonsterSizeX = stringList.get(0).length();
        seaMonster = new boolean[seaMonsterSizeY][seaMonsterSizeX];

        for (int y = 0; y < seaMonsterSizeY; y++) {
            for (int x = 0; x < seaMonsterSizeX; x++) {
                seaMonster[y][x] = stringList.get(y).charAt(x) == '#';
            }
        }
    }

    private static int countActivePixels(boolean[][] data) {
        int result = 0;
        int sizeY = data.length;
        int sizeX = data[0].length;

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                if (data[y][x]) {
                    result++;
                }
            }
        }

        return result;
    }

    private int countSeaMonsters() {
        int result = 0;

        for (int i = 0; i < 2; i++) {
            result = countSeaMonstersForOrientation();

            if (result > 0) {
                return result;
            }

            for (int j = 1; j < 4; j++) {
                rotateRight(image);
                result = countSeaMonstersForOrientation();

                if (result > 0) {
                    return result;
                }
            }

            flipHorizontal(image);
        }

        return result;
    }

    private int countSeaMonstersForOrientation() {
        int result = 0;

        for (int imgY = 0; imgY <= (imageSize - seaMonsterSizeY); imgY++) {
            PIXEL: for (int imgX = 0; imgX <= (imageSize - seaMonsterSizeX); imgX++) {
                for (int smY = 0; smY < seaMonsterSizeY; smY++) {
                    for (int smX = 0; smX < seaMonsterSizeX; smX++) {
                        if (seaMonster[smY][smX]) {
                            if (!image[imgY + smY][imgX + smX]) {
                                continue PIXEL;
                            }
                        }
                    }
                }

                result++;
            }
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        buildTileGrid(resPath);
        return tileGrid[0][0].id * tileGrid[tileGridMaxIdx][0].id * tileGrid[0][tileGridMaxIdx].id
                * tileGrid[tileGridMaxIdx][tileGridMaxIdx].id;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        buildTileGrid(resPath);
        buildImage();
        buildSeaMonster();
        int imageActivePixels = countActivePixels(image);
        int seaMonsterActivePixels = countActivePixels(seaMonster);
        int seaMonsters = countSeaMonsters();
        return imageActivePixels - (seaMonsters * seaMonsterActivePixels);
    }

    public static void main(String[] args) {
        new Aoc20Day20().execute(args);
    }
}
