package aoc.y2020.day08;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2020.Aoc20Day;

public class Aoc20Day08 extends Aoc20Day {

    private static final Pattern LINE_PATTERN = Pattern.compile("([a-z]{3}) ([+-]\\d+)");

    private int accumulator;

    private int address;

    private enum Command {
        acc, jmp, nop;

        static Command getByName(String name) {
            for (Command value : values()) {
                if (value.name().equals(name)) {
                    return value;
                }
            }

            return nop;
        }
    }

    private static class CodeLine {
        Command command;
        final int argument;
        boolean visited = false;

        public CodeLine(Command cmd, int arg) {
            command = cmd;
            argument = arg;
        }
    }

    public List<CodeLine> buildProgram(String resPath) {
        List<CodeLine> result = new ArrayList<>();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            try {
                Command cmd = Command.getByName(matcher.group(1));
                int arg = Integer.parseInt(matcher.group(2));
                result.add(new CodeLine(cmd, arg));
            } catch (NumberFormatException e) {
                logError("Parsing integers in '" + matcher.group() + "' failed", e);
            }
        }

        return result;
    }

    private void runProgram(List<CodeLine> program) {
        accumulator = 0;
        address = 0;
        CodeLine codeLine = program.get(address);

        for (CodeLine line : program) {
            line.visited = false;
        }

        while (!codeLine.visited) {
            switch (codeLine.command) {
            case acc:
                accumulator += codeLine.argument;
                address++;
                break;
            case jmp:
                address += codeLine.argument;
                break;
            default:
                address++;
            }

            if ((address < 0) || (address >= program.size())) {
                return;
            }

            codeLine.visited = true;
            codeLine = program.get(address);
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        List<CodeLine> program = buildProgram(resPath);
        runProgram(program);
        return accumulator;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<CodeLine> program = buildProgram(resPath);

        for (CodeLine codeLine : program) {
            switch (codeLine.command) {
            case jmp:
                codeLine.command = Command.nop;
                runProgram(program);
                codeLine.command = Command.jmp;
                break;
            case nop:
                codeLine.command = Command.jmp;
                runProgram(program);
                codeLine.command = Command.nop;
                break;
            default:
                continue;
            }

            if (address == program.size()) {
                return accumulator;
            }
        }

        return 0;
    }

    public static void main(String[] args) {
        new Aoc20Day08().execute(args);
    }
}
