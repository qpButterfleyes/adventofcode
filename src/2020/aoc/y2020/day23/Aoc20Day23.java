package aoc.y2020.day23;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import aoc.y2020.Aoc20Day;

public class Aoc20Day23 extends Aoc20Day {

    private static class Cup {

        final int label;

        Cup next = null;

        Cup(int lbl) {
            label = lbl;
        }
    }

    private Cup[] cupArray;

    private Cup destinationCup = null;

    private void createCup(int label) {
        cupArray[label] = new Cup(label);
    }

    private void insertCup(int label) {
        Cup cup = cupArray[label];
        destinationCup.next = cup;
        destinationCup = cup;
    }

    private Cup pickCupAfter(Cup cup) {
        Cup cupToPick = cup.next;
        cup.next = cupToPick.next;
        return cupToPick;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        List<Integer> cupList = readStringList(resPath).get(0).chars().mapToObj(i -> i - '0')
                .collect(Collectors.toCollection(LinkedList::new));

        for (int i = 1; i <= 100; i++) {
            int currentCup = cupList.remove(0);
            List<Integer> pickedCups = Arrays.asList(cupList.remove(0), cupList.remove(0), cupList.remove(0));
            int destinationCup = currentCup;

            do {
                destinationCup = destinationCup > 1 ? destinationCup - 1 : 9;
            } while (!cupList.contains(destinationCup));

            cupList.addAll(cupList.indexOf(destinationCup) + 1, pickedCups);
            cupList.add(currentCup);
        }

        while (cupList.get(0).intValue() != 1) {
            cupList.add(cupList.remove(0));
        }

        StringBuilder sb = new StringBuilder();
        IntStream.range(0, 9).forEach(i -> sb.append(cupList.get(i)));
        return Long.parseLong(sb.toString());
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        cupArray = new Cup[1_000_001];
        IntStream.rangeClosed(1, 1_000_000).forEach(this::createCup);
        String cupString = readStringList(resPath).get(0);
        int currentLabel = cupString.charAt(0) - '0';
        Cup currentCup = cupArray[currentLabel];
        destinationCup = currentCup;
        IntStream.range(1, 9).map(i -> cupString.charAt(i) - '0').forEach(this::insertCup);
        IntStream.rangeClosed(10, 1_000_000).forEach(this::insertCup);
        destinationCup.next = currentCup;

        for (int round = 1; round <= 10_000_000; round++) {
            List<Cup> pickedCups = Arrays.asList(pickCupAfter(currentCup), pickCupAfter(currentCup),
                    pickCupAfter(currentCup));
            int destinationLabel = currentCup.label;

            do {
                destinationLabel = destinationLabel > 1 ? destinationLabel - 1 : 1_000_000;
                destinationCup = cupArray[destinationLabel];
            } while (pickedCups.contains(destinationCup));

            Cup cupAfterInsert = destinationCup.next;
            pickedCups.stream().map(c -> c.label).forEach(this::insertCup);
            destinationCup.next = cupAfterInsert;
            currentCup = currentCup.next;
        }

        Cup cupNextTo1 = cupArray[1].next;
        return 1L * cupNextTo1.label * cupNextTo1.next.label;
    }

    public static void main(String[] args) {
        new Aoc20Day23().execute(args);
    }
}
