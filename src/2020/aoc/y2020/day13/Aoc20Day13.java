package aoc.y2020.day13;

import java.util.List;

import aoc.y2020.Aoc20Day;

public class Aoc20Day13 extends Aoc20Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        List<String> stringList = readStringList(resPath, "[,x]+", false, true);
        int arrivalTime = Integer.parseInt(stringList.remove(0));
        int[] busIDs = stringList.stream().mapToInt(Integer::parseInt).toArray();
        int bestWaitTime = Integer.MAX_VALUE;
        int bestBusID = 0;

        for (int id : busIDs) {
            int waitTime = id - (arrivalTime % id);

            if (bestWaitTime > waitTime) {
                bestWaitTime = waitTime;
                bestBusID = id;
            }
        }

        return bestWaitTime * bestBusID;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<String> stringList = readStringList(resPath, ",", false, true);
        stringList.remove(0);
        long increment = Integer.parseInt(stringList.get(0));
        long departure = increment;

        for (int busIndex = 1; busIndex < stringList.size(); busIndex++) {
            String busIDString = stringList.get(busIndex);

            if (!"x".equals(busIDString)) {
                int busID = Integer.parseInt(busIDString);

                while (((departure + busIndex) % busID) != 0) {
                    departure += increment;
                }

                increment *= busID;
            }
        }

        return departure;
    }

    public static void main(String[] args) {
        new Aoc20Day13().execute(args);
    }
}
