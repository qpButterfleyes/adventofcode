package aoc.y2020.day22;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import aoc.y2020.Aoc20Day;

public class Aoc20Day22 extends Aoc20Day {

    private List<List<Integer>> decks;

    private void buildDecks(String resPath) {
        decks = new ArrayList<>();
        List<List<String>> stringGroupList = readStringGroupList(resPath, null);

        for (List<String> stringGroup : stringGroupList) {
            stringGroup.remove(0);
            decks.add(stringGroup.stream().map(Integer::valueOf).collect(Collectors.toCollection(LinkedList::new)));
        }
    }

    private int playGame1(List<Integer> deck0, List<Integer> deck1) {
        while (!deck0.isEmpty() && !deck1.isEmpty()) {
            int card0 = deck0.remove(0);
            int card1 = deck1.remove(0);

            if (card0 > card1) {
                deck0.add(card0);
                deck0.add(card1);
            } else {
                deck1.add(card1);
                deck1.add(card0);
            }
        }

        return deck0.isEmpty() ? 1 : 0;
    }

    private int playGame2(List<Integer> deck0, List<Integer> deck1) {
        Set<List<List<Integer>>> gameStates = new HashSet<>();

        while (!deck0.isEmpty() && !deck1.isEmpty()) {
            List<List<Integer>> gameState = Arrays.asList(new ArrayList<>(deck0), new ArrayList<>(deck1));

            if (!gameStates.add(gameState)) {
                return 0;
            }

            int card0 = deck0.remove(0);
            int card1 = deck1.remove(0);
            boolean roundWonBy0 = card0 > card1;

            if ((deck0.size() >= card0) && (deck1.size() >= card1)) {
                List<Integer> subGameDeck0 = new LinkedList<>(deck0.subList(0, card0));
                List<Integer> subGameDeck1 = new LinkedList<>(deck1.subList(0, card1));
                roundWonBy0 = playGame2(subGameDeck0, subGameDeck1) == 0;
            }

            if (roundWonBy0) {
                deck0.add(card0);
                deck0.add(card1);
            } else {
                deck1.add(card1);
                deck1.add(card0);
            }
        }

        return deck0.isEmpty() ? 1 : 0;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        buildDecks(resPath);
        List<Integer> winnerDeck = decks.get(playGame1(decks.get(0), decks.get(1)));
        int cardCount = winnerDeck.size();
        return IntStream.range(0, cardCount).map(i -> (cardCount - i) * winnerDeck.get(i)).sum();
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        buildDecks(resPath);
        List<Integer> winnerDeck = decks.get(playGame2(decks.get(0), decks.get(1)));
        int cardCount = winnerDeck.size();
        return IntStream.range(0, cardCount).map(i -> (cardCount - i) * winnerDeck.get(i)).sum();
    }

    public static void main(String[] args) {
        new Aoc20Day22().execute(args);
    }
}
