package aoc.y2020.day16;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2020.Aoc20Day;

public class Aoc20Day16 extends Aoc20Day {

    private static final Pattern RULE_PATTERN = Pattern.compile("([a-z ]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)");

    private static final Pattern TICKET_PATTERN = Pattern.compile("(\\d+),?");

    private static class Rule {
        final String fieldName;
        final int range1Min;
        final int range1Max;
        final int range2Min;
        final int range2Max;

        Rule(String field, int r1Min, int r1Max, int r2Min, int r2Max) {
            fieldName = field;
            range1Min = r1Min;
            range1Max = r1Max;
            range2Min = r2Min;
            range2Max = r2Max;
        }

        boolean matches(Integer value) {
            return ((value >= range1Min) && (value <= range1Max)) || ((value >= range2Min) && (value <= range2Max));
        }

        boolean matches(List<Integer> values) {
            return values.stream().allMatch(this::matches);
        }

        @Override
        public String toString() {
            return fieldName;
        }
    }

    private List<Rule> ruleList;

    private List<Integer> myTicket;

    private List<List<Integer>> nearbyTickets;

    private int errorRate;

    private void validateTickets(String resPath) {
        ruleList = new ArrayList<>();
        myTicket = null;
        nearbyTickets = new ArrayList<>();
        errorRate = 0;
        List<String> stringList = readStringList(resPath);

        for (String line : stringList) {
            Matcher ruleMatcher = RULE_PATTERN.matcher(line);
            Matcher ticketMatcher = TICKET_PATTERN.matcher(line);

            if (line.endsWith(":")) {
                continue;
            } else if (ruleMatcher.matches()) {
                ruleList.add(new Rule(ruleMatcher.group(1), Integer.parseInt(ruleMatcher.group(2)),
                        Integer.parseInt(ruleMatcher.group(3)), Integer.parseInt(ruleMatcher.group(4)),
                        Integer.parseInt(ruleMatcher.group(5))));
            } else {
                List<Integer> ticket = new ArrayList<>();

                while (ticketMatcher.find()) {
                    ticket.add(Integer.parseInt(ticketMatcher.group(1)));
                }

                if (myTicket == null) {
                    myTicket = ticket;
                } else {
                    nearbyTickets.add(ticket);
                }
            }
        }

        Iterator<List<Integer>> ticketIterator = nearbyTickets.iterator();

        while (ticketIterator.hasNext()) {
            List<Integer> ticket = ticketIterator.next();
            boolean foundError = false;

            for (Integer value : ticket) {
                boolean foundMatchingRule = false;

                for (Rule rule : ruleList) {
                    if (rule.matches(value)) {
                        foundMatchingRule = true;
                        break;
                    }
                }

                if (!foundMatchingRule) {
                    foundError = true;
                    errorRate += value;
                }
            }

            if (foundError) {
                ticketIterator.remove();
            }
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        validateTickets(resPath);
        return errorRate;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        validateTickets(resPath);
        Map<Integer, List<Rule>> matchingRulesMap = new HashMap<>();
        Map<String, Integer> myTicketMap = new HashMap<>();

        for (int i = 0; i < myTicket.size(); i++) {
            int valueIndex = i;
            Integer myValue = myTicket.get(i);
            List<Integer> sameFieldValues = new ArrayList<>();
            sameFieldValues.add(myValue);
            nearbyTickets.stream().map(t -> t.get(valueIndex)).forEach(v -> sameFieldValues.add(v));
            List<Rule> matchingRules = matchingRulesMap.computeIfAbsent(myValue, ArrayList::new);
            ruleList.stream().filter(r -> r.matches(sameFieldValues)).forEach(r -> matchingRules.add(r));
            //System.out.println("\t" + myValue + "\t" + matchingRules);
        }

        while (!matchingRulesMap.isEmpty()) {
            Integer myValue = null;

            for (Entry<Integer, List<Rule>> entry : matchingRulesMap.entrySet()) {
                List<Rule> ruleList = entry.getValue();

                if (ruleList.size() == 1) {
                    myValue = entry.getKey();
                    Rule rule = ruleList.get(0);
                    myTicketMap.put(rule.fieldName, myValue);
                    //System.out.println("\t" + myValue + "\t" + rule.fieldName);
                    matchingRulesMap.values().stream().forEach(rules -> rules.remove(rule));
                    break;
                }
            }

            if (myValue != null) {
                matchingRulesMap.remove(myValue);
            } else {
                break;
            }
        }

        return myTicketMap.entrySet().stream().filter(e -> e.getKey().startsWith("departure"))
                .mapToLong(Entry::getValue).reduce((i, j) -> i * j).orElse(0);
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample1");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample2");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc20Day16().execute(args);
    }
}
