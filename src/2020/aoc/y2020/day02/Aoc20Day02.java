package aoc.y2020.day02;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2020.Aoc20Day;

public class Aoc20Day02 extends Aoc20Day {

    private static final Pattern LINE_PATTERN = Pattern.compile("(\\d+)-(\\d+) ([a-z]): ([a-z]+)");

    @Override
    public long getPuzzle1Long(String resPath) {
        int result = 0;
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            try {
                int min = Integer.parseInt(matcher.group(1));
                int max = Integer.parseInt(matcher.group(2));
                char charToTest = matcher.group(3).charAt(0);
                String pw = matcher.group(4);
                long count = pw.chars().filter(ch -> ch == charToTest).count();

                if ((min <= count) && (count <= max)) {
                    result++;
                }
            } catch (NumberFormatException e) {
                logError("Parsing integers in '" + matcher.group() + "' failed", e);
            }
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int result = 0;
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            try {
                int pos1 = Integer.parseInt(matcher.group(1)) - 1;
                int pos2 = Integer.parseInt(matcher.group(2)) - 1;
                char charToTest = matcher.group(3).charAt(0);
                String pw = matcher.group(4);

                if ((pw.charAt(pos1) == charToTest) ^ (pw.charAt(pos2) == charToTest)) {
                    result++;
                }
            } catch (NumberFormatException e) {
                logError("Parsing integers in '" + matcher.group() + "' failed", e);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        new Aoc20Day02().execute(args);
    }
}
