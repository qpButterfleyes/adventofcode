package aoc.y2020.day03;

import java.util.List;

import aoc.y2020.Aoc20Day;

public class Aoc20Day03 extends Aoc20Day {

    private boolean[][] buildMap(String resPath) {
        List<String> mapLines = readStringList(resPath);
        int colCount = mapLines.get(0).length();
        boolean[][] map = new boolean[mapLines.size()][colCount];

        for (int y = 0; y < map.length; y++) {
            for (int x = 0; x < colCount; x++) {
                map[y][x] = mapLines.get(y).charAt(x) == '#';
            }
        }

        return map;
    }

    private static int traverseMap(boolean[][] map, int xStep, int yStep) {
        int hitCount = 0;
        int colCount = map[0].length;
        int x = 0;

        for (int y = 0; y < map.length; y += yStep) {
            if (map[y][x]) {
                hitCount++;
            }

            x = (x + xStep) % colCount;
        }

        return hitCount;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        boolean[][] map = buildMap(resPath);
        return traverseMap(map, 3, 1);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int result = 1;
        boolean[][] map = buildMap(resPath);
        result *= traverseMap(map, 1, 1);
        result *= traverseMap(map, 3, 1);
        result *= traverseMap(map, 5, 1);
        result *= traverseMap(map, 7, 1);
        result *= traverseMap(map, 1, 2);
        return result;
    }

    public static void main(String[] args) {
        new Aoc20Day03().execute(args);
    }
}
