package aoc.y2020.day18;

import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2020.Aoc20Day;

public class Aoc20Day18 extends Aoc20Day {

    private static final Pattern SUM_PATTERN = Pattern.compile("(\\d+)\\s*\\+\\s*(\\d+)");

    private static final Pattern PRODUCT_PATTERN = Pattern.compile("(\\d+)\\s*\\*\\s*(\\d+)");

    private static final Pattern PAREN_PRODUCT_PATTERN = Pattern
            .compile("\\((\\d+)\\s*\\*\\s*(\\d+)((\\s*\\*\\s*\\d+)*)\\)");

    private static final Pattern PAREN_NUMBER_PATTERN = Pattern.compile("\\((\\d+)\\)");

    private long eval1(String expression) {
        long result = 0;
        int parenLevel = 0;
        int openPos = 0;
        int closePos = 0;
        boolean adding = false;
        boolean multiplying = false;

        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);
            long currentNumber = 0;

            if (ch == '(') {
                parenLevel++;

                if (parenLevel == 1) {
                    openPos = i;
                }
            } else if (ch == ')') {
                parenLevel--;

                if (parenLevel == 0) {
                    closePos = i;
                    currentNumber = eval1(expression.substring(openPos + 1, closePos));
                }
            }

            if (parenLevel > 0) {
                continue;
            }

            if (('1' <= ch) && (ch <= '9')) {
                currentNumber = ch - 0x30;
            } else if (ch == '+') {
                adding = true;
            } else if (ch == '*') {
                multiplying = true;
            }

            if (currentNumber > 0) {
                if (adding) {
                    result += currentNumber;
                    adding = false;
                } else if (multiplying) {
                    result *= currentNumber;
                    multiplying = false;
                } else {
                    result = currentNumber;
                }
            }
        }

        return result;
    }

    private long eval2(String expression) {
        boolean matchFound;

        do {
            matchFound = false;
            Matcher matcher = SUM_PATTERN.matcher(expression);

            if (matcher.find()) {
                expression = matcher.replaceFirst(
                        String.valueOf(Long.parseLong(matcher.group(1)) + Long.parseLong(matcher.group(2))));
                matchFound = true;
            }

            matcher = PAREN_PRODUCT_PATTERN.matcher(expression);

            if (matcher.find()) {
                expression = matcher
                        .replaceFirst("(" + (Long.parseLong(matcher.group(1)) * Long.parseLong(matcher.group(2)))
                                + matcher.group(3) + ")");
                matchFound = true;
            }

            matcher = PAREN_NUMBER_PATTERN.matcher(expression);

            if (matcher.find()) {
                expression = matcher.replaceFirst(matcher.group(1));
                matchFound = true;
            }
        } while (matchFound);

        do {
            matchFound = false;
            Matcher matcher = PRODUCT_PATTERN.matcher(expression);

            if (matcher.find()) {
                expression = matcher.replaceFirst(
                        String.valueOf(Long.parseLong(matcher.group(1)) * Long.parseLong(matcher.group(2))));
                matchFound = true;
            }
        } while (matchFound);

        return Long.parseLong(expression);
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        long result = 0;
        Logger logger = getLogger();
        List<String> stringList = readStringList(resPath);

        for (String expression : stringList) {
            long value = eval1(expression);
            logger.fine("\t" + value);
            result += value;
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        long result = 0;
        Logger logger = getLogger();
        List<String> stringList = readStringList(resPath);

        for (String expression : stringList) {
            long value = eval2(expression);
            logger.fine("\t" + value);
            result += value;
        }

        return result;
    }

    public static void main(String[] args) {
        new Aoc20Day18().execute(args);
    }
}
