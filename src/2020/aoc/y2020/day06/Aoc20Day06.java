package aoc.y2020.day06;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import aoc.y2020.Aoc20Day;

public class Aoc20Day06 extends Aoc20Day {

    private static Set<Integer> getChars(String str) {
        return str.chars().mapToObj(Integer::valueOf).collect(Collectors.toSet());
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        int result = 0;
        List<List<String>> groupList = readStringGroupList(resPath, null);

        for (List<String> group : groupList) {
            Set<Integer> chars = new HashSet<>();

            for (String answer : group) {
                chars.addAll(getChars(answer));
            }

            result += chars.size();
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int result = 0;
        List<List<String>> groupList = readStringGroupList(resPath, null);

        for (List<String> group : groupList) {
            Set<Integer> chars = getChars(group.get(0));

            for (String answer : group) {
                chars.retainAll(getChars(answer));
            }

            result += chars.size();
        }

        return result;
    }

    public static void main(String[] args) {
        new Aoc20Day06().execute(args);
    }
}
