package aoc.y2020.day01;

import aoc.y2020.Aoc20Day;

public class Aoc20Day01 extends Aoc20Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        int[] numbers = readStringList(resPath).stream().mapToInt(Integer::parseInt).toArray();

        for (int i = 0; i < numbers.length; i++) {
            int x = numbers[i];

            for (int j = i + 1; j < numbers.length; j++) {
                int y = numbers[j];

                if ((x + y) == 2020) {
                    return x * y;
                }
            }
        }

        return 0;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int[] numbers = readStringList(resPath).stream().mapToInt(Integer::parseInt).toArray();

        for (int i = 0; i < numbers.length; i++) {
            int x = numbers[i];

            for (int j = i + 1; j < numbers.length; j++) {
                int y = numbers[j];

                for (int k = j + 1; k < numbers.length; k++) {
                    int z = numbers[k];

                    if ((x + y + z) == 2020) {
                        return x * y * z;
                    }
                }
            }
        }

        return 0;
    }

    public static void main(String[] args) {
        new Aoc20Day01().execute(args);
    }
}
