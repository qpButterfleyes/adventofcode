package aoc.y2020.day24;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import aoc.y2020.Aoc20Day;

public class Aoc20Day24 extends Aoc20Day {

    enum Step {

        E(1, 0), SE(1, -1), SW(0, -1), W(-1, 0), NW(-1, 1), NE(0, 1);

        final int x;

        final int y;

        Step(int x, int y) {
            this.x = x;
            this.y = y;
        }

        static Step getByName(String name) {
            for (Step value : values()) {
                if (value.name().equalsIgnoreCase(name)) {
                    return value;
                }
            }

            return null;
        }
    }

    public List<List<Step>> buildPathList(String resPath) {
        List<List<Step>> result = new ArrayList<>();
        List<String> pathStringList = readStringList(resPath);

        for (String pathString : pathStringList) {
            List<Step> path = new ArrayList<>();
            result.add(path);
            int index = 0;

            while (index < pathString.length()) {
                char ch1 = pathString.charAt(index);
                index++;

                switch (ch1) {
                case 'e':
                    path.add(Step.E);
                    break;
                case 'w':
                    path.add(Step.W);
                    break;
                case 's':
                case 'n':
                    char ch2 = pathString.charAt(index);
                    index++;

                    switch (ch2) {
                    case 'e':
                        path.add(ch1 == 's' ? Step.SE : Step.NE);
                        break;
                    case 'w':
                        path.add(ch1 == 's' ? Step.SW : Step.NW);
                        break;
                    default:
                    }
                default:
                }
            }
        }

        return result;
    }

    private Set<Dimension> buildFlippedTilesSet(String resPath) {
        Set<Dimension> flippedTiles = new HashSet<>();
        List<List<Step>> pathList = buildPathList(resPath);

        for (List<Step> path : pathList) {
            int x = 0;
            int y = 0;

            for (Step step : path) {
                x += step.x;
                y += step.y;
            }

            Dimension tile = new Dimension(x, y);

            if (flippedTiles.contains(tile)) {
                flippedTiles.remove(tile);
            } else {
                flippedTiles.add(tile);
            }
        }

        return flippedTiles;
    }

    private static void countFlippedNeighbors(boolean[][] tileMap, int[][] neighborCountMap) {
        int gridSize = tileMap.length;

        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                neighborCountMap[y][x] = 0;

                for (Step step : Step.values()) {
                    int nbX = x + step.x;
                    int nbY = y + step.y;

                    if ((nbY >= 0) && (nbY < gridSize) && (nbX >= 0) && (nbX < gridSize) && tileMap[nbY][nbX]) {
                        neighborCountMap[y][x]++;
                    }
                }
            }
        }
    }

    private static void setNextState(boolean[][] tileMap, int[][] neighborCountMap) {
        int gridSize = tileMap.length;

        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                if (!tileMap[y][x] && (neighborCountMap[y][x] == 2)) {
                    tileMap[y][x] = true;
                } else if (tileMap[y][x] && ((neighborCountMap[y][x] < 1) || (neighborCountMap[y][x] > 2))) {
                    tileMap[y][x] = false;
                }
            }
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        return buildFlippedTilesSet(resPath).size();
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int result = 0;
        Set<Dimension> initialFlippedTiles = buildFlippedTilesSet(resPath);
        int gridOffset = initialFlippedTiles.stream().mapToInt(t -> Math.max(Math.abs(t.width), Math.abs(t.height)))
                .max().orElse(0) + 100;
        int gridSize = 2 * gridOffset;
        boolean[][] flippedTiles = new boolean[gridSize][gridSize];
        int[][] flippedNeighborCounts = new int[gridSize][gridSize];

        for (Dimension tile : initialFlippedTiles) {
            flippedTiles[gridOffset + tile.height][gridOffset + tile.width] = true;
        }

        for (int day = 0; day < 100; day++) {
            countFlippedNeighbors(flippedTiles, flippedNeighborCounts);
            setNextState(flippedTiles, flippedNeighborCounts);
        }

        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                if (flippedTiles[y][x]) {
                    result++;
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        new Aoc20Day24().execute(args);
    }
}
