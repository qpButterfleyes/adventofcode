package aoc.y2020.day12;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2020.Aoc20Day;

public class Aoc20Day12 extends Aoc20Day {

    private static final Pattern LINE_PATTERN = Pattern.compile("([A-Z])(\\d+)");

    private enum Command {
        UNDEFINED, E, S, W, N, L, R, F;

        static Command getByName(String name) {
            for (Command value : values()) {
                if (value.name().equals(name)) {
                    return value;
                }
            }

            return UNDEFINED;
        }
    }

    private static class Line {
        Command command;
        final int argument;

        public Line(Command cmd, int arg) {
            command = cmd;
            argument = arg;
        }
    }

    private static Command getForwardDirection(int orientation) {
        switch (orientation) {
        case 0:
            return Command.E;
        case 90:
            return Command.S;
        case 180:
            return Command.W;
        case 270:
            return Command.N;
        default:
            return Command.UNDEFINED;
        }
    }

    public List<Line> buildScript(String resPath) {
        List<Line> result = new ArrayList<>();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            try {
                Command cmd = Command.getByName(matcher.group(1));
                int arg = Integer.parseInt(matcher.group(2));
                result.add(new Line(cmd, arg));
            } catch (NumberFormatException e) {
                logError("Parsing integers in '" + matcher.group() + "' failed", e);
            }
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        int orientation = 0;
        int posX = 0;
        int posY = 0;
        List<Line> script = buildScript(resPath);

        for (Line line : script) {
            Command cmd = line.command == Command.F ? getForwardDirection(orientation) : line.command;
            int arg = line.argument;

            switch (cmd) {
            case E:
                posX += arg;
                break;
            case S:
                posY += arg;
                break;
            case W:
                posX -= arg;
                break;
            case N:
                posY -= arg;
                break;
            case L:
                orientation = (orientation + (360 - arg)) % 360;
                break;
            case R:
                orientation = (orientation + arg) % 360;
                break;
            default:
            }
        }

        return Math.abs(posX) + Math.abs(posY);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int waypointX = 10;
        int waypointY = -1;
        int posX = 0;
        int posY = 0;
        List<Line> script = buildScript(resPath);

        for (Line line : script) {
            Command cmd = line.command;
            int arg = line.argument;

            if (cmd == Command.L) {
                cmd = Command.R;
                arg = 360 - arg;
            }

            switch (cmd) {
            case E:
                waypointX += arg;
                break;
            case S:
                waypointY += arg;
                break;
            case W:
                waypointX -= arg;
                break;
            case N:
                waypointY -= arg;
                break;
            case R:
                switch (arg) {
                case 90:
                    int newY = waypointX;
                    waypointX = -waypointY;
                    waypointY = newY;
                    break;
                case 180:
                    waypointX = -waypointX;
                    waypointY = -waypointY;
                    break;
                case 270:
                    int newX = waypointY;
                    waypointY = -waypointX;
                    waypointX = newX;
                    break;
                default:
                }

                break;
            case F:
                posX += arg * waypointX;
                posY += arg * waypointY;
                break;
            default:
            }
        }

        return Math.abs(posX) + Math.abs(posY);
    }

    public static void main(String[] args) {
        new Aoc20Day12().execute(args);
    }
}
