package aoc.y2020.day17;

import java.util.List;

import aoc.y2020.Aoc20Day;

public class Aoc20Day17 extends Aoc20Day {

    private static final int CYCLE_COUNT = 6;

    private int xSize = 0;

    private int ySize = 0;

    private int zSize = (2 * CYCLE_COUNT) + 1;

    private int wSize = (2 * CYCLE_COUNT) + 1;

    private boolean[][][][] buildMap(String resPath) {
        List<String> mapLines = readStringList(resPath);
        int rowCount = mapLines.size();
        int colCount = mapLines.get(0).length();
        ySize = (2 * CYCLE_COUNT) + rowCount;
        xSize = (2 * CYCLE_COUNT) + colCount;
        boolean[][][][] map = new boolean[wSize][zSize][ySize][xSize];

        for (int y = 0; y < rowCount; y++) {
            for (int x = 0; x < colCount; x++) {
                if (mapLines.get(y).charAt(x) == '#') {
                    map[CYCLE_COUNT][CYCLE_COUNT][CYCLE_COUNT + y][CYCLE_COUNT + x] = true;
                }
            }
        }

        return map;
    }

    private void countActiveNeighbors(boolean[][][][] map, int[][][][] neighborCountMap) {
        for (int w = 0; w < wSize; w++) {
            countActiveNeighbors(map, neighborCountMap, w);
        }
    }

    private void countActiveNeighbors(boolean[][][][] map, int[][][][] neighborCountMap, int w) {
        for (int z = 0; z < zSize; z++) {
            for (int y = 0; y < ySize; y++) {
                for (int x = 0; x < xSize; x++) {
                    neighborCountMap[w][z][y][x] = 0;

                    for (int nbW = Math.max(0, w - 1); nbW < Math.min(w + 2, wSize); nbW++) {
                        for (int nbZ = Math.max(0, z - 1); nbZ < Math.min(z + 2, zSize); nbZ++) {
                            for (int nbY = Math.max(0, y - 1); nbY < Math.min(y + 2, ySize); nbY++) {
                                for (int nbX = Math.max(0, x - 1); nbX < Math.min(x + 2, xSize); nbX++) {
                                    if (map[nbW][nbZ][nbY][nbX]
                                            && ((nbW != w) || (nbZ != z) || (nbY != y) || (nbX != x))) {
                                        neighborCountMap[w][z][y][x] += 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void setNextState(boolean[][][][] map, int[][][][] neighborCountMap) {
        for (int w = 0; w < wSize; w++) {
            setNextState(map, neighborCountMap, w);
        }
    }

    private void setNextState(boolean[][][][] map, int[][][][] neighborCountMap, int w) {
        for (int z = 0; z < zSize; z++) {
            for (int y = 0; y < ySize; y++) {
                for (int x = 0; x < xSize; x++) {
                    map[w][z][y][x] = (map[w][z][y][x] && (neighborCountMap[w][z][y][x] == 2))
                            || (neighborCountMap[w][z][y][x] == 3);
                }
            }
        }
    }

    private long countActiveCubes(boolean[][][][] map) {
        int result = 0;

        for (int w = 0; w < wSize; w++) {
            result += countActiveCubes(map, w);
        }

        return result;
    }

    private long countActiveCubes(boolean[][][][] map, int w) {
        int result = 0;

        for (int z = 0; z < zSize; z++) {
            for (int y = 0; y < ySize; y++) {
                for (int x = 0; x < xSize; x++) {
                    if (map[w][z][y][x]) {
                        result++;
                    }
                }
            }
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        boolean[][][][] map = buildMap(resPath);
        int[][][][] neighborCountMap = new int[wSize][zSize][ySize][xSize];

        for (int i = 0; i < CYCLE_COUNT; i++) {
            countActiveNeighbors(map, neighborCountMap, CYCLE_COUNT);
            setNextState(map, neighborCountMap, CYCLE_COUNT);
        }

        return countActiveCubes(map, CYCLE_COUNT);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        boolean[][][][] map = buildMap(resPath);
        int[][][][] neighborCountMap = new int[wSize][zSize][ySize][xSize];

        for (int i = 0; i < CYCLE_COUNT; i++) {
            countActiveNeighbors(map, neighborCountMap);
            setNextState(map, neighborCountMap);
        }

        return countActiveCubes(map);
    }

    public static void main(String[] args) {
        new Aoc20Day17().execute(args);
    }
}
