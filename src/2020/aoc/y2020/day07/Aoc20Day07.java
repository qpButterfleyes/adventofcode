package aoc.y2020.day07;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2020.Aoc20Day;

public class Aoc20Day07 extends Aoc20Day {

    private static final Pattern INNER_BAGS_PATTERN = Pattern.compile("([1-9]) ([a-z]+ [a-z]+) bags?");

    private static final Pattern LINE_PATTERN = Pattern
            .compile("([a-z]+ [a-z]+) bags contain (no other bags|((" + INNER_BAGS_PATTERN.pattern() + "(, )?)+)).");

    private Map<String, Map<String, Integer>> buildRuleMap(String resPath) {
        Map<String, Map<String, Integer>> ruleMap = new HashMap<>();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            String outerBag = matcher.group(1);
            Map<String, Integer> innerBagMap = new HashMap<>();
            ruleMap.put(outerBag, innerBagMap);
            String innerBags = matcher.group(3);

            if (innerBags != null) {
                Matcher containedMatcher = INNER_BAGS_PATTERN.matcher(innerBags);

                while (containedMatcher.find()) {
                    try {
                        String innerBagType = containedMatcher.group(2);
                        Integer innerBagCount = Integer.valueOf(containedMatcher.group(1));
                        innerBagMap.put(innerBagType, innerBagCount);
                    } catch (NumberFormatException e) {
                        logError("Parsing the string '" + innerBags + "' failed", e);
                    }
                }
            }
        }

        return ruleMap;
    }

    private static boolean mayContain(String outerBag, String innerBag, Map<String, Map<String, Integer>> ruleMap) {
        Set<String> childBags = ruleMap.get(outerBag).keySet();

        if (childBags.contains(innerBag)) {
            return true;
        }

        for (String childBag : childBags) {
            if (mayContain(childBag, innerBag, ruleMap)) {
                return true;
            }
        }

        return false;
    }

    private static int countInnerBags(String outerBag, Map<String, Map<String, Integer>> ruleMap) {
        int result = 0;
        Map<String, Integer> childBagMap = ruleMap.get(outerBag);

        for (Entry<String, Integer> entry : childBagMap.entrySet()) {
            String childBagType = entry.getKey();
            int childBagCount = entry.getValue();
            result += childBagCount * (1 + countInnerBags(childBagType, ruleMap));
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        int result = 0;
        Map<String, Map<String, Integer>> ruleMap = buildRuleMap(resPath);

        for (String outerBag : ruleMap.keySet()) {
            if (mayContain(outerBag, "shiny gold", ruleMap)) {
                result++;
            }
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        Map<String, Map<String, Integer>> ruleMap = buildRuleMap(resPath);
        return countInnerBags("shiny gold", ruleMap);
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample1");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample1");
        executePuzzle(2, "Sample2");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc20Day07().execute(args);
    }
}
