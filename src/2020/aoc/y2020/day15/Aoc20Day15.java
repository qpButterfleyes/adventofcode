package aoc.y2020.day15;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import aoc.y2020.Aoc20Day;

public class Aoc20Day15 extends Aoc20Day {

    protected static final String PKEY_SOLUTION = "solution";

    @Override
    protected void parseArg(String key, String value) {
        switch (key) {
        case "s":
        case PKEY_SOLUTION:
            if ((value != null) && !value.isEmpty()) {
                CMDLINE_ARGS.setProperty(PKEY_SOLUTION, value);
            }

            break;
        default:
            parseArg(key, value);
        }
    }

    private long solution1(String resPath, int iterationCount) {
        List<String> startingNumList = readStringList(resPath, ",", false, true);
        List<Integer> numbers = startingNumList.stream().map(Integer::valueOf).collect(Collectors.toList());
        int nextNumber = 0;

        for (int index = numbers.size(); index < (iterationCount - 1); index++) {
            int lastIndex = numbers.lastIndexOf(nextNumber);
            numbers.add(nextNumber);
            nextNumber = lastIndex < 0 ? 0 : index - lastIndex;
        }

        return nextNumber;
    }

    private long solution2(String resPath, int iterationCount) {
        List<String> startingNumList = readStringList(resPath, ",", false, true);
        int startingNumCount = startingNumList.size();
        Map<Integer, Integer> numbers = new HashMap<>();
        IntStream.rangeClosed(1, startingNumCount)
                .forEach(i -> numbers.put(Integer.valueOf(startingNumList.get(i - 1)), i));
        int nextNumber = 0;

        for (int index = startingNumCount + 1; index < iterationCount; index++) {
            Integer lastIndex = numbers.get(nextNumber);
            numbers.put(nextNumber, index);
            nextNumber = lastIndex == null ? 0 : index - lastIndex;
        }

        return nextNumber;
    }

    private long solution3(String resPath, int iterationCount) {
        List<String> startingNumList = readStringList(resPath, ",", false, true);
        int startingNumCount = startingNumList.size();
        int[] numbers = new int[iterationCount];
        IntStream.rangeClosed(1, startingNumCount)
                .forEach(i -> numbers[Integer.parseInt(startingNumList.get(i - 1))] = i);
        int nextNumber = 0;

        for (int index = startingNumCount + 1; index < iterationCount; index++) {
            int lastIndex = numbers[nextNumber];
            numbers[nextNumber] = index;
            nextNumber = lastIndex == 0 ? 0 : index - lastIndex;
        }

        return nextNumber;
    }

    private long selectedSolution(String resPath, int iterationCount) {
        String solution = CMDLINE_ARGS.getProperty(PKEY_SOLUTION, "3");

        switch (solution) {
        case "1":
            return solution1(resPath, iterationCount);
        case "2":
            return solution2(resPath, iterationCount);
        default:
            return solution3(resPath, iterationCount);
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        return selectedSolution(resPath, 2020);
    }

    // The straightforward solution1 implementation is far too slow for 30M iterations.
    // The map-based solution2 is way faster but still requires some patience.
    // The array-based solution3 still takes visible time, but just about a second per sample.
    @Override
    public long getPuzzle2Long(String resPath) {
        return selectedSolution(resPath, 30_000_000);
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample1");
        executePuzzle(1, "Sample2");
        executePuzzle(1, "Sample3");
        executePuzzle(1, "Sample4");
        executePuzzle(1, "Sample5");
        executePuzzle(1, "Sample6");
        executePuzzle(1, "Sample7");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample1");
        executePuzzle(2, "Sample2");
        executePuzzle(2, "Sample3");
        executePuzzle(2, "Sample4");
        executePuzzle(2, "Sample5");
        executePuzzle(2, "Sample6");
        executePuzzle(2, "Sample7");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc20Day15().execute(args);
    }
}
