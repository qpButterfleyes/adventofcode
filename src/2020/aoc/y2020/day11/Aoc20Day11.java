package aoc.y2020.day11;

import java.util.List;

import aoc.y2020.Aoc20Day;

public class Aoc20Day11 extends Aoc20Day {

    private enum Direction {
        NE(1, -1), E(1, 0), SE(1, 1), S(0, 1), SW(-1, 1), W(-1, 0), NW(-1, -1), N(0, -1);

        final int x;
        final int y;

        Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private Boolean[][] buildSeatMap(String resPath) {
        List<String> mapLines = readStringList(resPath);
        int colCount = mapLines.get(0).length();
        Boolean[][] map = new Boolean[mapLines.size()][colCount];

        for (int y = 0; y < map.length; y++) {
            for (int x = 0; x < colCount; x++) {
                if (mapLines.get(y).charAt(x) == 'L') {
                    map[y][x] = Boolean.FALSE;
                }
            }
        }

        return map;
    }

    private static void countOccupiedNeighbors1(Boolean[][] seatMap, int[][] neighborCountMap) {
        int colCount = seatMap[0].length;
        int rowCount = seatMap.length;

        for (int y = 0; y < rowCount; y++) {
            for (int x = 0; x < colCount; x++) {
                if (seatMap[y][x] != null) {
                    neighborCountMap[y][x] = 0;

                    for (Direction d : Direction.values()) {
                        int nbX = x + d.x;
                        int nbY = y + d.y;

                        if ((nbY >= 0) && (nbY < rowCount) && (nbX >= 0) && (nbX < colCount)
                                && (Boolean.TRUE.equals(seatMap[nbY][nbX]))) {
                            neighborCountMap[y][x] += 1;
                        }
                    }
                }
            }
        }
    }

    private static void countOccupiedNeighbors2(Boolean[][] seatMap, int[][] neighborCountMap) {
        int colCount = seatMap[0].length;
        int rowCount = seatMap.length;

        for (int y = 0; y < rowCount; y++) {
            for (int x = 0; x < colCount; x++) {
                if (seatMap[y][x] != null) {
                    neighborCountMap[y][x] = 0;

                    for (Direction d : Direction.values()) {
                        for (int distance = 1;; distance++) {
                            int nbX = x + (distance * d.x);
                            int nbY = y + (distance * d.y);

                            if ((nbY < 0) || (nbY >= rowCount) || (nbX < 0) || (nbX >= colCount)) {
                                break;
                            }

                            if (seatMap[nbY][nbX] != null) {
                                if (seatMap[nbY][nbX]) {
                                    neighborCountMap[y][x] += 1;
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private static boolean setNextState(Boolean[][] seatMap, int[][] neighborCountMap, int maxNeighbors) {
        boolean changing = false;

        for (int y = 0; y < seatMap.length; y++) {
            for (int x = 0; x < seatMap[0].length; x++) {
                if (seatMap[y][x] != null) {
                    if (!seatMap[y][x] && (neighborCountMap[y][x] == 0)) {
                        seatMap[y][x] = Boolean.TRUE;
                        changing = true;
                    } else if (seatMap[y][x] && (neighborCountMap[y][x] > maxNeighbors)) {
                        seatMap[y][x] = Boolean.FALSE;
                        changing = true;
                    }
                }
            }
        }

        return changing;
    }

    private long countOccupiedSeats(Boolean[][] seatMap) {
        int result = 0;

        for (int y = 0; y < seatMap.length; y++) {
            for (int x = 0; x < seatMap[0].length; x++) {
                if (Boolean.TRUE.equals(seatMap[y][x])) {
                    result++;
                }
            }
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        Boolean[][] seatMap = buildSeatMap(resPath);
        int[][] neighborCountMap = new int[seatMap.length][seatMap[0].length];
        boolean changing = true;

        while (changing) {
            countOccupiedNeighbors1(seatMap, neighborCountMap);
            changing = setNextState(seatMap, neighborCountMap, 3);
        }

        return countOccupiedSeats(seatMap);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        Boolean[][] seatMap = buildSeatMap(resPath);
        int[][] neighborCountMap = new int[seatMap.length][seatMap[0].length];
        boolean changing = true;

        while (changing) {
            countOccupiedNeighbors2(seatMap, neighborCountMap);
            changing = setNextState(seatMap, neighborCountMap, 4);
        }

        return countOccupiedSeats(seatMap);
    }

    public static void main(String[] args) {
        new Aoc20Day11().execute(args);
    }
}
