package aoc.y2020.day05;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import aoc.y2020.Aoc20Day;

public class Aoc20Day05 extends Aoc20Day {

    private Stream<String> getEncodedIDStream(String resPath) {
        return readStringList(resPath).stream();
    }

    private static long decode(String encodedID) {
        try {
            return Long.parseLong(encodedID.replaceAll("[FL]", "0").replaceAll("[BR]", "1"), 2);
        } catch (NumberFormatException e) {
            logError(getLogger(Aoc20Day05.class), "Parsing the string '" + encodedID + "' failed", e);
            return 0;
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        return getEncodedIDStream(resPath).mapToLong(Aoc20Day05::decode).max().orElse(0);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<Long> idList = getEncodedIDStream(resPath).map(Aoc20Day05::decode).collect(Collectors.toList());
        long maxID = getPuzzle1Long(resPath);

        for (long i = maxID; i > 0; i--) {
            if (!idList.contains(i)) {
                return i;
            }
        }

        return 0;
    }

    public static void main(String[] args) {
        new Aoc20Day05().execute(args);
    }
}
