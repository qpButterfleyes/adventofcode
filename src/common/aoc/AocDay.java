package aoc;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class AocDay {

    private static final Pattern DAY_CLASS_PATTERN = Pattern.compile("Aoc(\\d{2})Day(\\d{2})");

    private static final Pattern ARG_PATTERN = Pattern.compile("-([0-9A-Za-z]+):(.+)");

    protected static final Properties CMDLINE_ARGS = new Properties();

    protected static final String PKEY_LOGLEVEL = "loglevel";

    protected void parseArgs(String[] args) {
        for (String arg : args) {
            Matcher matcher = ARG_PATTERN.matcher(arg);

            if (matcher.matches()) {
                String key = matcher.group(1);
                String value = matcher.group(2);

                switch (key) {
                case "l":
                case PKEY_LOGLEVEL:
                    if ((value != null) && !value.isEmpty()) {
                        CMDLINE_ARGS.setProperty(PKEY_LOGLEVEL, value.toUpperCase());
                    }

                    break;
                default:
                    parseArg(key, value);
                }
            }
        }
    }

    protected void parseArg(String key, String value) {
        // override where needed
    }

    private static class StdOutConsoleHandler extends ConsoleHandler {
        StdOutConsoleHandler() {
            setOutputStream(System.out);
        }
    }

    private static Logger commonLogger = null;

    public Logger getLogger() {
        return getLogger(getClass());
    }

    public static synchronized Logger getLogger(Class<?> resLoaderClass) {
        if (commonLogger == null) {
            commonLogger = initLogger(resLoaderClass);
        }

        return commonLogger;
    }

    private static Logger initLogger(Class<?> resLoaderClass) {
        Level level = Level.parse(CMDLINE_ARGS.getProperty(PKEY_LOGLEVEL, "INFO"));
        Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        logger.setUseParentHandlers(false);
        logger.setLevel(level);
        Handler consoleHandler = new StdOutConsoleHandler();
        consoleHandler.setFormatter(new RelativeTimeLogFormatter());
        consoleHandler.setLevel(level);
        logger.addHandler(consoleHandler);
        String year = resLoaderClass.getName().substring(5, 9);
        File logDir = new File("test", year);
        logDir.mkdirs();

        try {
            Handler fileHandler = new FileHandler(logDir + "/" + resLoaderClass.getSimpleName() + ".log");
            fileHandler.setFormatter(new RelativeTimeLogFormatter());
            fileHandler.setLevel(level);
            logger.addHandler(fileHandler);
        } catch (SecurityException | IOException e) {
            logError(logger, "Creating file log handler failed", e);
        }

        return logger;
    }

    public static void logError(Logger logger, String msg, Throwable t) {
        logger.log(Level.SEVERE, msg, t);
    }

    public void logError(String msg, Throwable t) {
        logError(getLogger(), msg, t);
    }

    public List<String> readStringList(String resPath) {
        return readStringList(resPath, null, false, true);
    }

    public List<String> readStringList(String resPath, String entrySeparator, boolean keepBlankLines,
            boolean trimLines) {
        List<String> lines = new ArrayList<>();

        try (InputStream inStream = getClass().getResourceAsStream(resPath);
                BufferedReader reader = new BufferedReader(new InputStreamReader(inStream, StandardCharsets.UTF_8))) {
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                if (trimLines) {
                    line = line.trim();
                }

                if (!line.isEmpty()) {
                    if ((entrySeparator != null) && !entrySeparator.isEmpty()) {
                        Arrays.stream(line.split(entrySeparator)).filter(s -> !s.isEmpty()).forEach(lines::add);
                    } else {
                        lines.add(line);
                    }
                } else if (keepBlankLines) {
                    lines.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }

    public List<List<String>> readStringGroupList(String resPath, String entrySeparator) {
        List<String> lines = readStringList(resPath, entrySeparator, true, true);
        List<List<String>> groupList = new ArrayList<>();
        List<String> group = new ArrayList<>();

        for (String line : lines) {
            if (!line.isEmpty()) {
                group.add(line);
            } else if (!group.isEmpty()) {
                groupList.add(group);
                group = new ArrayList<>();
            }
        }

        if (!group.isEmpty()) {
            groupList.add(group);
        }

        return groupList;
    }

    public List<Matcher> buildMatcherList(String resPath, Pattern pattern) {
        List<String> lines = readStringList(resPath);
        return buildMatcherList(lines, pattern);
    }

    public List<Matcher> buildMatcherList(List<String> lines, Pattern pattern) {
        return lines.stream().map(line -> pattern.matcher(line)).filter(Matcher::matches).collect(Collectors.toList());
    }

    public long getPuzzle1Long(String resPath) {
        // Override in the day subclasses.
        return 0;
    }

    public long getPuzzle2Long(String resPath) {
        // Override in the day subclasses.
        return 0;
    }

    public String getPuzzle1String(String resPath) {
        // Override in day subclasses where the puzzle solution is not numeric.
        return String.valueOf(getPuzzle1Long(resPath));
    }

    public String getPuzzle2String(String resPath) {
        // Override in day subclasses where the puzzle solution is not numeric.
        return String.valueOf(getPuzzle2Long(resPath));
    }

    public void executePuzzle(int puzzleIdx, String inputName) {
        Logger logger = getLogger();
        String className = getClass().getSimpleName();
        String resPath = className + inputName + ".txt";

        try {
            String solution = puzzleIdx == 1 ? getPuzzle1String(resPath) : getPuzzle2String(resPath);
            int day = Integer.parseInt(className.substring(className.length() - 2));
            logger.info(String.format("%2d.%d %-16s %s", day, puzzleIdx, inputName + ":", solution));
        } catch (Exception e) {
            logError("Execution puzzle " + puzzleIdx + " failed for input '" + resPath + "'", e);
        }
    }

    public void executeDay() {
        executePuzzle(1, "Sample");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample");
        executePuzzle(2, "Input");
    }

    private void executeSeason() {
        Logger logger = getLogger();
        String seasonPackage = getClass().getPackage().getName();
        String seasonClassName = getClass().getSimpleName();
        int day = 0;

        try {
            while (true) {
                day++;
                String dayClassName = String.format("%s.day%02d.%s%02d", seasonPackage, day, seasonClassName, day);
                Class<?> dayClass = Class.forName(dayClassName);
                dayClass.getMethod("executeDay").invoke(dayClass.newInstance());
                logger.info("");
            }
        } catch (ClassNotFoundException e) {
            logger.info("Done.");
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | InstantiationException e) {
            logError(logger, "Call by reflection failed for day " + day, e);
        }
    }

    public void execute(String[] args) {
        parseArgs(args);
        Matcher matcher = DAY_CLASS_PATTERN.matcher(getClass().getSimpleName());

        if (matcher.matches()) {
            executeDay();
        } else {
            executeSeason();
        }
    }
}
