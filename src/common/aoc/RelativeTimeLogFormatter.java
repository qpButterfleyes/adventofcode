package aoc;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public class RelativeTimeLogFormatter extends SimpleFormatter {

    private static final String format = "%1$tH:%1$tM:%1$tS.%1$tL\t%5$s%6$s%n";

    private static final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

    private static final long startTime = System.currentTimeMillis();

    @Override
    public synchronized String format(LogRecord record) {
        calendar.setTimeInMillis(record.getMillis() - startTime);
        String source;

        if (record.getSourceClassName() != null) {
            source = record.getSourceClassName();

            if (record.getSourceMethodName() != null) {
                source += " " + record.getSourceMethodName();
            }
        } else {
            source = record.getLoggerName();
        }

        String message = formatMessage(record);
        String throwable = "";

        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            pw.println();
            record.getThrown().printStackTrace(pw);
            pw.close();
            throwable = sw.toString();
        }

        if (message.isEmpty() && throwable.isEmpty()) {
            return "\n";
        }

        return String.format(format, calendar, source, record.getLoggerName(), record.getLevel().getName(), message,
                throwable);
    }
}
