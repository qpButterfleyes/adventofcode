package aoc.y2015.day15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day15 extends Aoc15Day {

    private static final Pattern LINE_PATTERN = Pattern.compile(
            "[A-Za-z]+: capacity (-?\\d+), durability (-?\\d+), flavor (-?\\d+), texture (-?\\d+), calories (-?\\d+)");

    private static final int PROPERTY_COUNT = 5;

    private final List<int[]> ingredientList = new ArrayList<>();

    private void buildIngredientList(String resPath) {
        ingredientList.clear();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            int[] ingredient = new int[PROPERTY_COUNT];
            ingredientList.add(ingredient);

            for (int i = 0; i < PROPERTY_COUNT; i++) {
                ingredient[i] = Integer.parseInt(matcher.group(i + 1));
            }
        }
    }

    private long getMaxScore(List<int[]> ingredients, int teaspoonCount, int[] scoreArray, Integer fixedCalories) {
        List<int[]> remainingIngredients = new ArrayList<>(ingredients);
        int[] ingredient = remainingIngredients.remove(0);

        if (remainingIngredients.isEmpty()) {
            return computeScore(ingredient, teaspoonCount, scoreArray, fixedCalories);
        }

        long result = 0;
        int[] newScoreArray = Arrays.copyOf(scoreArray, PROPERTY_COUNT);

        for (int tsp = 0; tsp <= teaspoonCount; tsp++) {
            for (int i = 0; i < PROPERTY_COUNT; i++) {
                newScoreArray[i] = scoreArray[i] + (tsp * ingredient[i]);
            }

            long tmpResult = getMaxScore(remainingIngredients, teaspoonCount - tsp, newScoreArray, fixedCalories);

            if (result < tmpResult) {
                result = tmpResult;
            }
        }

        return result;
    }

    private long computeScore(int[] ingredient, int teaspoonCount, int[] scoreArray, Integer fixedCalories) {
        int[] newScoreArray = Arrays.copyOf(scoreArray, PROPERTY_COUNT);

        for (int i = 0; i < PROPERTY_COUNT; i++) {
            newScoreArray[i] = scoreArray[i] + (teaspoonCount * ingredient[i]);
        }

        if ((fixedCalories != null) && (fixedCalories != newScoreArray[4])) {
            return 0;
        }

        long result = 1;

        for (int i = 0; i < 4; i++) {
            if (newScoreArray[i] <= 0) {
                return 0;
            }

            result *= newScoreArray[i];
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        buildIngredientList(resPath);
        return getMaxScore(ingredientList, 100, new int[PROPERTY_COUNT], null);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        buildIngredientList(resPath);
        return getMaxScore(ingredientList, 100, new int[PROPERTY_COUNT], 500);
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc15Day15().execute(args);
    }
}
