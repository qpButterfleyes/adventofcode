package aoc.y2015.day12;

import java.util.Arrays;
import java.util.List;

import aoc.y2015.Aoc15Day;

public class Aoc15Day12 extends Aoc15Day {

    private String filterRedValues(String str) {
        StringBuilder sb = new StringBuilder();
        int braceLevel = 0;
        int openBracePos = -1;

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            switch (ch) {
            case '{':
                if (braceLevel == 0) {
                    openBracePos = i;
                }

                braceLevel++;
                break;
            case '}':
                braceLevel--;

                if (braceLevel == 0) {
                    String filteredSubString = filterRedValues(str.substring(openBracePos + 1, i));

                    if (!filteredSubString.isEmpty()) {
                        sb.append('{').append(filteredSubString).append('}');
                    }
                }

                break;
            default:
                if (braceLevel == 0) {
                    if (str.startsWith(":\"red\"", i)) {
                        return "";
                    }

                    sb.append(ch);
                }
            }
        }

        return sb.toString();
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        List<String> stringList = readStringList(resPath, "[^-0-9]+", false, true);
        return stringList.stream().mapToLong(Long::parseLong).sum();
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = filterRedValues(stringList.get(0));
        return Arrays.stream(str.split("[^-0-9]+")).filter(s -> !s.isEmpty()).mapToLong(Long::parseLong).sum();
    }

    public static void main(String[] args) {
        new Aoc15Day12().execute(args);
    }
}
