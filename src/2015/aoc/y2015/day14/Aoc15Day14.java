package aoc.y2015.day14;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day14 extends Aoc15Day {

    private static final Pattern LINE_PATTERN = Pattern
            .compile("[A-Za-z]+ can fly (\\d+) km/s for (\\d+) seconds, but then must rest for (\\d+) seconds\\.");

    private static class Reindeer {

        int speed;

        int flyTime;

        int restTime;

        int distance = 0;

        int score = 0;
    }

    private final List<Reindeer> reindeerList = new ArrayList<>();

    private void buildReindeerList(String resPath) {
        reindeerList.clear();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            Reindeer reindeer = new Reindeer();
            reindeer.speed = Integer.parseInt(matcher.group(1));
            reindeer.flyTime = Integer.parseInt(matcher.group(2));
            reindeer.restTime = Integer.parseInt(matcher.group(3));
            reindeerList.add(reindeer);
        }
    }

    private int race1(int raceDuration) {
        int maxDistance = 0;

        for (Reindeer reindeer : reindeerList) {
            int fullIntervalTime = reindeer.flyTime + reindeer.restTime;
            int fullIntervalCount = raceDuration / fullIntervalTime;
            int finalFlyTime = Math.min(reindeer.flyTime, raceDuration % fullIntervalTime);
            reindeer.distance = ((fullIntervalCount * reindeer.flyTime) + finalFlyTime) * reindeer.speed;

            if (maxDistance < reindeer.distance) {
                maxDistance = reindeer.distance;
            }
        }

        return maxDistance;
    }

    private int race2(int raceDuration) {
        for (int i = 1; i <= raceDuration; i++) {
            int maxDistance = race1(i);
            reindeerList.stream().filter(r -> r.distance == maxDistance).forEach(r -> r.score++);
        }

        return reindeerList.stream().mapToInt(r -> r.score).max().orElse(0);
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        buildReindeerList(resPath);
        return race1(2503);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        buildReindeerList(resPath);
        return race2(2503);
    }

    public static void main(String[] args) {
        new Aoc15Day14().execute(args);
    }
}
