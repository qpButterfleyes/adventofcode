package aoc.y2015.day17;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import aoc.y2015.Aoc15Day;

public class Aoc15Day17 extends Aoc15Day {

    private int[] getGroupedCombinationCounts(String resPath) {
        List<Integer> intList = readStringList(resPath).stream().map(Integer::valueOf).collect(Collectors.toList());
        int allContainers = intList.size();
        int[] result = new int[allContainers];
        long combinationCount = Math.round(Math.pow(2, allContainers));

        COMBINATION: for (long i = 0; i < combinationCount; i++) {
            int sum = 0;
            String binaryStr = Long.toBinaryString(i);
            int charCount = binaryStr.length();
            int offset = allContainers - charCount;
            int usedContainers = 0;

            for (int j = 0; j < charCount; j++) {
                if (binaryStr.charAt(j) == '1') {
                    sum += intList.get(j + offset);
                    usedContainers++;

                    if (sum > 150) {
                        continue COMBINATION;
                    }
                }
            }

            if (sum == 150) {
                result[usedContainers]++;
            }
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        return Arrays.stream(getGroupedCombinationCounts(resPath)).sum();
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int[] groupedCombinationCounts = getGroupedCombinationCounts(resPath);

        for (int i = 0; i < groupedCombinationCounts.length; i++) {
            if (groupedCombinationCounts[i] > 0) {
                return groupedCombinationCounts[i];
            }
        }

        return 0;
    }

    public static void main(String[] args) {
        new Aoc15Day17().execute(args);
    }
}
