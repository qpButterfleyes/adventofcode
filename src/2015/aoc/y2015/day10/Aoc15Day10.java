package aoc.y2015.day10;

import java.util.List;

import aoc.y2015.Aoc15Day;

public class Aoc15Day10 extends Aoc15Day {

    private static long processString(String str, int iterations) {
        for (int i = 0; i < iterations; i++) {
            StringBuilder sb = new StringBuilder();
            char ch = str.charAt(0);
            int charCount = 1;

            for (int j = 1; j < str.length(); j++) {
                if (str.charAt(j) == ch) {
                    charCount++;
                } else {
                    sb.append(charCount).append(ch);
                    ch = str.charAt(j);
                    charCount = 1;
                }
            }

            str = sb.append(charCount).append(ch).toString();
        }

        return str.length();
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        return processString(str, 40);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        return processString(str, 50);
    }

    public static void main(String[] args) {
        new Aoc15Day10().execute(args);
    }
}
