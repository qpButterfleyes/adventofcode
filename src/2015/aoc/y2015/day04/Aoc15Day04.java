package aoc.y2015.day04;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import aoc.y2015.Aoc15Day;

public class Aoc15Day04 extends Aoc15Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        long suffix = 0;

        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] hashBytes;

            do {
                suffix++;
                digest.update((str + suffix).getBytes());
                hashBytes = digest.digest();
            } while ((hashBytes[0] != 0) || (hashBytes[1] != 0) || ((hashBytes[2] & 0xFF) > 0x0F));
        } catch (NoSuchAlgorithmException e) {
            logError("Creating message digest failed", e);
        }

        return suffix;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        long suffix = 0;

        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] hashBytes;

            do {
                suffix++;
                digest.update((str + suffix).getBytes());
                hashBytes = digest.digest();
            } while ((hashBytes[0] != 0) || (hashBytes[1] != 0) || (hashBytes[2] != 0));
        } catch (NoSuchAlgorithmException e) {
            logError("Creating message digest failed", e);
        }

        return suffix;
    }

    public static void main(String[] args) {
        new Aoc15Day04().execute(args);
    }
}
