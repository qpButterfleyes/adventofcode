package aoc.y2015;

import aoc.AocDay;

public class Aoc15Day extends AocDay {

    @Override
    public void executeDay() {
        executePuzzle(1, "Input");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc15Day().execute(args);
    }
}
