package aoc.y2015.day03;

import java.awt.Dimension;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import aoc.y2015.Aoc15Day;

public class Aoc15Day03 extends Aoc15Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        int x = 0;
        int y = 0;
        Set<Dimension> giftedHouses = new HashSet<>(Arrays.asList(new Dimension(0, 0)));

        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
            case '^':
                x--;
                break;
            case 'v':
                x++;
                break;
            case '>':
                y++;
                break;
            case '<':
                y--;
                break;
            default:
            }

            giftedHouses.add(new Dimension(x, y));
        }

        return giftedHouses.size();
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        int[] x = new int[2];
        int[] y = new int[2];
        int santaIdx = 0;
        Set<Dimension> giftedHouses = new HashSet<>(Arrays.asList(new Dimension(0, 0)));

        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
            case '^':
                x[santaIdx]--;
                break;
            case 'v':
                x[santaIdx]++;
                break;
            case '>':
                y[santaIdx]++;
                break;
            case '<':
                y[santaIdx]--;
                break;
            default:
            }

            giftedHouses.add(new Dimension(x[santaIdx], y[santaIdx]));
            santaIdx = 1 - santaIdx;
        }

        return giftedHouses.size();
    }

    public static void main(String[] args) {
        new Aoc15Day03().execute(args);
    }
}
