package aoc.y2015.day18;

import java.util.List;

import aoc.y2015.Aoc15Day;

public class Aoc15Day18 extends Aoc15Day {

    private enum Direction {
        NE(1, -1), E(1, 0), SE(1, 1), S(0, 1), SW(-1, 1), W(-1, 0), NW(-1, -1), N(0, -1);

        final int x;
        final int y;

        Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private int gridSize;

    private boolean[][] lightGrid;

    private int[][] neighborCountMap;

    private void buildGrid(String resPath) {
        List<String> lines = readStringList(resPath);
        gridSize = lines.size();
        lightGrid = new boolean[gridSize][gridSize];
        neighborCountMap = new int[gridSize][gridSize];

        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                lightGrid[y][x] = lines.get(y).charAt(x) == '#';
            }
        }
    }

    private void countActiveNeighbors() {
        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                neighborCountMap[y][x] = 0;

                for (Direction d : Direction.values()) {
                    int nbX = x + d.x;
                    int nbY = y + d.y;

                    if ((nbY >= 0) && (nbY < gridSize) && (nbX >= 0) && (nbX < gridSize) && lightGrid[nbY][nbX]) {
                        neighborCountMap[y][x]++;
                    }
                }
            }
        }
    }

    private void setNextState() {
        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                if (neighborCountMap[y][x] == 3) {
                    lightGrid[y][x] = true;
                } else if (neighborCountMap[y][x] != 2) {
                    lightGrid[y][x] = false;
                }
            }
        }
    }

    private void setCornerLightsActive() {
        int max = gridSize - 1;
        lightGrid[0][0] = true;
        lightGrid[0][max] = true;
        lightGrid[max][0] = true;
        lightGrid[max][max] = true;
    }

    private long countActiveLights() {
        int result = 0;

        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                if (lightGrid[y][x]) {
                    result++;
                }
            }
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        buildGrid(resPath);

        for (int i = 0; i < 100; i++) {
            countActiveNeighbors();
            setNextState();
        }

        return countActiveLights();
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        buildGrid(resPath);
        setCornerLightsActive();

        for (int i = 0; i < 100; i++) {
            countActiveNeighbors();
            setNextState();
            setCornerLightsActive();
        }

        return countActiveLights();
    }

    public static void main(String[] args) {
        new Aoc15Day18().execute(args);
    }
}
