package aoc.y2015.day06;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day06 extends Aoc15Day {

    private static final Pattern LINE_PATTERN = Pattern
            .compile("(turn on|turn off|toggle) (\\d+),(\\d+) through (\\d+),(\\d+)");

    private enum Command {
        UNDEFINED(""), ON("turn on"), OFF("turn off"), TOGGLE("toggle");

        final String cmdName;

        Command(String name) {
            cmdName = name;
        }

        static Command getByName(String name) {
            for (Command value : values()) {
                if (value.cmdName.equals(name)) {
                    return value;
                }
            }

            return UNDEFINED;
        }
    }

    private static class ScriptLine {
        final Command command;
        final int left;
        final int top;
        final int right;
        final int bottom;

        ScriptLine(Command cmd, int l, int t, int r, int b) {
            command = cmd;
            left = l;
            top = t;
            right = r;
            bottom = b;
        }
    }

    private List<ScriptLine> buildScript(String resPath) {
        List<ScriptLine> script = new ArrayList<>();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            try {
                Command cmd = Command.getByName(matcher.group(1));
                int left = Integer.parseInt(matcher.group(2));
                int top = Integer.parseInt(matcher.group(3));
                int right = Integer.parseInt(matcher.group(4));
                int bottom = Integer.parseInt(matcher.group(5));
                script.add(new ScriptLine(cmd, left, top, right, bottom));
            } catch (NumberFormatException e) {
                logError("Parsing integers in '" + matcher.group() + "' failed", e);
            }
        }

        return script;
    }

    private final int size = 1000;

    @Override
    public long getPuzzle1Long(String resPath) {
        int result = 0;
        boolean[][] lights = new boolean[size][size];
        List<ScriptLine> script = buildScript(resPath);

        for (ScriptLine line : script) {
            for (int y = line.top; y <= line.bottom; y++) {
                for (int x = line.left; x <= line.right; x++) {
                    switch (line.command) {
                    case ON:
                        lights[y][x] = true;
                        break;
                    case OFF:
                        lights[y][x] = false;
                        break;
                    case TOGGLE:
                        lights[y][x] = !lights[y][x];
                        break;
                    default:
                    }
                }
            }
        }

        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                if (lights[y][x]) {
                    result++;
                }
            }
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int result = 0;
        int[][] lights = new int[size][size];
        List<ScriptLine> script = buildScript(resPath);

        for (ScriptLine line : script) {
            for (int y = line.top; y <= line.bottom; y++) {
                for (int x = line.left; x <= line.right; x++) {
                    switch (line.command) {
                    case ON:
                        lights[y][x] += 1;
                        break;
                    case OFF:
                        if (lights[y][x] > 0) {
                            lights[y][x] -= 1;
                        }

                        break;
                    case TOGGLE:
                        lights[y][x] += 2;
                        break;
                    default:
                    }
                }
            }
        }

        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                result += lights[y][x];
            }
        }

        return result;
    }

    public static void main(String[] args) {
        new Aoc15Day06().execute(args);
    }
}
