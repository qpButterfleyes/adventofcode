package aoc.y2015.day05;

import java.util.List;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day05 extends Aoc15Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        int result = 0;
        Pattern includeRule1 = Pattern.compile("[aeiou]([^aeiou]*[aeiou]){2}");
        Pattern includeRule2 = Pattern
                .compile("aa|bb|cc|dd|ee|ff|gg|hh|ii|jj|kk|ll|mm|nn|oo|pp|qq|rr|ss|tt|uu|vv|ww|xx|yy|zz");
        Pattern excludeRule = Pattern.compile("ab|cd|pq|xy");
        List<String> stringList = readStringList(resPath);

        for (String word : stringList) {
            if (includeRule1.matcher(word).find() && includeRule2.matcher(word).find()
                    && !excludeRule.matcher(word).find()) {
                result++;
            }
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int result = 0;
        List<String> stringList = readStringList(resPath);

        for (String word : stringList) {
            boolean foundPattern = false;

            for (int i = 0; i < (word.length() - 3); i++) {
                for (int j = i + 2; j < (word.length() - 1); j++) {
                    if ((word.charAt(i) == word.charAt(j)) && (word.charAt(i + 1) == word.charAt(j + 1))) {
                        foundPattern = true;
                        break;
                    }
                }
            }

            if (!foundPattern) {
                continue;
            }

            foundPattern = false;

            for (int i = 0; i < (word.length() - 2); i++) {
                if (word.charAt(i) == word.charAt(i + 2)) {
                    foundPattern = true;
                    break;
                }
            }

            if (foundPattern) {
                result++;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        new Aoc15Day05().execute(args);
    }
}
