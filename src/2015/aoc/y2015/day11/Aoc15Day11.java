package aoc.y2015.day11;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day11 extends Aoc15Day {

    private static final long MIN_1_DIGIT = Long.parseLong("a", 36);

    private static final long MIN_2_DIGIT = Long.parseLong("aa", 36);

    private static final long MIN_3_DIGIT = Long.parseLong("aaa", 36);

    private static final long MIN_4_DIGIT = Long.parseLong("aaaa", 36);

    private static final long MIN_5_DIGIT = Long.parseLong("aaaaa", 36);

    private static final long MIN_6_DIGIT = Long.parseLong("aaaaaa", 36);

    private static final long MIN_7_DIGIT = Long.parseLong("aaaaaaa", 36);

    private static final long MAX_8_DIGIT = Long.parseLong("zzzzzzzz", 36);

    private static final Pattern CHARSET_PATTERN = Pattern.compile("[a-hjkmnp-z]+");

    private static final Pattern STRAIGHT_PATTERN = Pattern
            .compile("abc|bcd|cde|def|efg|fgh|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz");

    private static final Pattern PAIR_PATTERN = Pattern
            .compile("aa|bb|cc|dd|ee|ff|gg|hh|jj|kk|mm|nn|pp|qq|rr|ss|tt|uu|vv|ww|xx|yy|zz");

    private boolean isValidPw(String str) {
        if (!CHARSET_PATTERN.matcher(str).matches() || !STRAIGHT_PATTERN.matcher(str).find()) {
            return false;
        }

        Set<String> pairSet = new HashSet<>();
        Matcher pairMatcher = PAIR_PATTERN.matcher(str);

        while (pairMatcher.find()) {
            pairSet.add(pairMatcher.group());
        }

        return pairSet.size() > 1;
    }

    private String getNextValidPw(String str) {
        for (long i = Long.parseLong(str, 36) + 1; i < MAX_8_DIGIT; i++) {
            str = Long.toString(i, 36);

            if (str.endsWith("0000000")) {
                i += MIN_7_DIGIT;
                str = Long.toString(i, 36);
            } else if (str.endsWith("000000")) {
                i += MIN_6_DIGIT;
                str = Long.toString(i, 36);
            } else if (str.endsWith("00000")) {
                i += MIN_5_DIGIT;
                str = Long.toString(i, 36);
            } else if (str.endsWith("0000")) {
                i += MIN_4_DIGIT;
                str = Long.toString(i, 36);
            } else if (str.endsWith("000")) {
                i += MIN_3_DIGIT;
                str = Long.toString(i, 36);
            } else if (str.endsWith("00")) {
                i += MIN_2_DIGIT;
                str = Long.toString(i, 36);
            } else if (str.endsWith("0")) {
                i += MIN_1_DIGIT;
                str = Long.toString(i, 36);
            }

            if (isValidPw(str)) {
                return str;
            }
        }

        return null;
    }

    @Override
    public String getPuzzle1String(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        return getNextValidPw(str);
    }

    @Override
    public String getPuzzle2String(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        str = getNextValidPw(str);
        return getNextValidPw(str);
    }

    public static void main(String[] args) {
        new Aoc15Day11().execute(args);
    }
}
