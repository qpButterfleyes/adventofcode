package aoc.y2015.day08;

import java.util.List;

import aoc.y2015.Aoc15Day;

public class Aoc15Day08 extends Aoc15Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        long result = 0;
        List<String> stringList = readStringList(resPath);

        for (String str : stringList) {
            StringBuilder decodedSb = new StringBuilder();
            int encodedLength = str.length();

            for (int i = 1; i < (encodedLength - 1); i++) {
                char ch = str.charAt(i);

                if (ch == '\\') {
                    ch = str.charAt(++i);

                    if (ch == 'x') {
                        char hex1 = str.charAt(++i);
                        char hex2 = str.charAt(++i);
                        String hexStr = new String(new char[] { hex1, hex2 });
                        ch = (char) Integer.parseInt(hexStr, 16);
                    }
                }

                decodedSb.append(ch);
            }

            result += encodedLength - decodedSb.length();
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        long result = 0;
        List<String> stringList = readStringList(resPath);

        for (String str : stringList) {
            String encodedStr = "\"" + str.replace("\\", "\\\\").replace("\"", "\\\"") + "\"";
            result += encodedStr.length() - str.length();
        }

        return result;
    }

    public static void main(String[] args) {
        new Aoc15Day08().execute(args);
    }
}
