package aoc.y2015.day16;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day16 extends Aoc15Day {

    private static final Pattern PROPERTY_PATTERN = Pattern.compile("([a-z]+): (\\d+)");

    private static final Pattern LINE_PATTERN = Pattern.compile("Sue (\\d+):(( " + PROPERTY_PATTERN + "(,|$))+)");

    private final Map<String, Integer> profileMap = new HashMap<>();

    private final Map<Integer, Map<String, Integer>> sueMap = new HashMap<>();

    private void buildProfileMap() {
        profileMap.clear();
        List<Matcher> matcherList = buildMatcherList(getClass().getSimpleName() + "Profile.txt", PROPERTY_PATTERN);
        matcherList.stream().forEach(m -> profileMap.put(m.group(1), Integer.parseInt(m.group(2))));
    }

    private void buildSueMap(String resPath) {
        sueMap.clear();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher lineMatcher : matcherList) {
            Map<String, Integer> propertyMap = new HashMap<>();
            sueMap.put(Integer.parseInt(lineMatcher.group(1)), propertyMap);
            Matcher propertyMatcher = PROPERTY_PATTERN.matcher(lineMatcher.group(2));

            while (propertyMatcher.find()) {
                propertyMap.put(propertyMatcher.group(1), Integer.parseInt(propertyMatcher.group(2)));
            }
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        buildProfileMap();
        buildSueMap(resPath);

        SUE: for (Entry<Integer, Map<String, Integer>> sue : sueMap.entrySet()) {
            Map<String, Integer> propertyMap = sue.getValue();

            for (Entry<String, Integer> property : propertyMap.entrySet()) {
                if (property.getValue() != profileMap.get(property.getKey())) {
                    continue SUE;
                }
            }

            return sue.getKey();
        }

        return 0;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        buildProfileMap();
        buildSueMap(resPath);

        SUE: for (Entry<Integer, Map<String, Integer>> sue : sueMap.entrySet()) {
            Map<String, Integer> propertyMap = sue.getValue();

            for (Entry<String, Integer> property : propertyMap.entrySet()) {
                String key = property.getKey();
                int sueValue = property.getValue();
                int profileValue = profileMap.get(key);

                switch (key) {
                case "cats":
                case "trees":
                    if (sueValue <= profileValue) {
                        continue SUE;
                    }
                    break;
                case "pomeranians":
                case "goldfish":
                    if (sueValue >= profileValue) {
                        continue SUE;
                    }
                    break;
                default:
                    if (sueValue != profileValue) {
                        continue SUE;
                    }
                }
            }

            return sue.getKey();
        }

        return 0;
    }

    public static void main(String[] args) {
        new Aoc15Day16().execute(args);
    }
}
