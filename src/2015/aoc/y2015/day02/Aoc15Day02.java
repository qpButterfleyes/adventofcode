package aoc.y2015.day02;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day02 extends Aoc15Day {

    private static final Pattern LINE_PATTERN = Pattern.compile("(\\d+)x(\\d+)x(\\d+)");

    @Override
    public long getPuzzle1Long(String resPath) {
        int result = 0;
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            try {
                int[] dim = new int[3];
                dim[0] = Integer.parseInt(matcher.group(1));
                dim[1] = Integer.parseInt(matcher.group(2));
                dim[2] = Integer.parseInt(matcher.group(3));
                Arrays.sort(dim);
                result += (3 * dim[0] * dim[1]) + (2 * dim[0] * dim[2]) + (2 * dim[1] * dim[2]);
            } catch (NumberFormatException e) {
                logError("Parsing integers in '" + matcher.group() + "' failed", e);
            }
        }

        return result;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        int result = 0;
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            try {
                int[] dim = new int[3];
                dim[0] = Integer.parseInt(matcher.group(1));
                dim[1] = Integer.parseInt(matcher.group(2));
                dim[2] = Integer.parseInt(matcher.group(3));
                Arrays.sort(dim);
                result += (2 * dim[0]) + (2 * dim[1]) + (dim[0] * dim[1] * dim[2]);
            } catch (NumberFormatException e) {
                logError("Parsing integers in '" + matcher.group() + "' failed", e);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        new Aoc15Day02().execute(args);
    }
}
