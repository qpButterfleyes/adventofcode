package aoc.y2015.day01;

import java.util.List;

import aoc.y2015.Aoc15Day;

public class Aoc15Day01 extends Aoc15Day {

    @Override
    public long getPuzzle1Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        int upCount = str.replace(")", "").length();
        int downCount = str.replace("(", "").length();
        return upCount - downCount;
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<String> stringList = readStringList(resPath);
        String str = stringList.get(0);
        int floor = 0;

        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
            case '(':
                floor++;
                break;
            case ')':
                floor--;
                break;
            default:
            }

            if (floor < 0) {
                return i + 1;
            }
        }

        return 0;
    }

    public static void main(String[] args) {
        new Aoc15Day01().execute(args);
    }
}
