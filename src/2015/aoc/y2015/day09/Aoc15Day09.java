package aoc.y2015.day09;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day09 extends Aoc15Day {

    private static final Pattern LINE_PATTERN = Pattern.compile("([A-Za-z]+) to ([A-Za-z]+) = (\\d+)");

    private final Map<String, Map<String, Integer>> distanceMap = new HashMap<>();

    private final Set<String> nodeSet = new HashSet<>();

    private void buildDistanceMap(String resPath) {
        distanceMap.clear();
        nodeSet.clear();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            String node1 = matcher.group(1);
            String node2 = matcher.group(2);
            Integer distance = Integer.valueOf(matcher.group(3));
            Map<String, Integer> distancesForNode1 = distanceMap.computeIfAbsent(node1, n -> new HashMap<>());
            distancesForNode1.put(node2, distance);
            Map<String, Integer> distancesForNode2 = distanceMap.computeIfAbsent(node2, n -> new HashMap<>());
            distancesForNode2.put(node1, distance);
        }

        nodeSet.addAll(distanceMap.keySet());
        Map<String, Integer> distancesForNull = distanceMap.computeIfAbsent(null, n -> new HashMap<>());
        nodeSet.stream().forEach(n -> distancesForNull.put(n, 0));
    }

    private int getShortestRoute(String currentNode, Set<String> pendingNodes) {
        if (pendingNodes.isEmpty()) {
            return 0;
        }

        int result = Integer.MAX_VALUE;

        for (String node : pendingNodes) {
            Set<String> remainingNodes = new HashSet<>(pendingNodes);
            remainingNodes.remove(node);
            int routeViaNode = distanceMap.get(currentNode).get(node) + getShortestRoute(node, remainingNodes);

            if (result > routeViaNode) {
                result = routeViaNode;
            }
        }

        return result;
    }

    private int getLongestRoute(String currentNode, Set<String> pendingNodes) {
        if (pendingNodes.isEmpty()) {
            return 0;
        }

        int result = 0;

        for (String node : pendingNodes) {
            Set<String> remainingNodes = new HashSet<>(pendingNodes);
            remainingNodes.remove(node);
            int routeViaNode = distanceMap.get(currentNode).get(node) + getLongestRoute(node, remainingNodes);

            if (result < routeViaNode) {
                result = routeViaNode;
            }
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        buildDistanceMap(resPath);
        return getShortestRoute(null, nodeSet);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        buildDistanceMap(resPath);
        return getLongestRoute(null, nodeSet);
    }

    public static void main(String[] args) {
        new Aoc15Day09().execute(args);
    }
}
