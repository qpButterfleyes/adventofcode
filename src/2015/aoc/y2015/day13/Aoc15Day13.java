package aoc.y2015.day13;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day13 extends Aoc15Day {

    private static final Pattern LINE_PATTERN = Pattern
            .compile("([A-Za-z]+) would (gain|lose) (\\d+) happiness units by sitting next to ([A-Za-z]+)\\.");

    private final Map<String, Map<String, Integer>> gainMap = new HashMap<>();

    private String startPerson;

    private void buildGainMap(String resPath) {
        gainMap.clear();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);
        startPerson = matcherList.get(0).group(1);

        for (Matcher matcher : matcherList) {
            String person1 = matcher.group(1);
            String person2 = matcher.group(4);
            Integer gain = Integer.valueOf(matcher.group(3));

            if ("lose".equals(matcher.group(2))) {
                gain = -gain;
            }

            Map<String, Integer> gainForPerson1 = gainMap.computeIfAbsent(person1, p -> new HashMap<>());
            gainForPerson1.put(person2, gain);
        }
    }

    private void addMe() {
        Map<String, Integer> gainForMe = new HashMap<>();

        for (Entry<String, Map<String, Integer>> entry : gainMap.entrySet()) {
            gainForMe.put(entry.getKey(), 0);
            entry.getValue().put("me", 0);
        }

        gainMap.put("me", gainForMe);
    }

    private int getMaxGain(String currentPerson, Set<String> pendingPersons) {
        if (pendingPersons.isEmpty()) {
            return gainMap.get(currentPerson).get(startPerson) + gainMap.get(startPerson).get(currentPerson);
        }

        int result = Integer.MIN_VALUE;

        for (String nextPerson : pendingPersons) {
            Set<String> remainingPersons = new HashSet<>(pendingPersons);
            remainingPersons.remove(nextPerson);
            int gain = gainMap.get(currentPerson).get(nextPerson) + gainMap.get(nextPerson).get(currentPerson)
                    + getMaxGain(nextPerson, remainingPersons);

            if (result < gain) {
                result = gain;
            }
        }

        return result;
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        buildGainMap(resPath);
        Set<String> remainingPersons = new HashSet<>(gainMap.keySet());
        remainingPersons.remove(startPerson);
        return getMaxGain(startPerson, remainingPersons);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        buildGainMap(resPath);
        addMe();
        Set<String> remainingPersons = new HashSet<>(gainMap.keySet());
        remainingPersons.remove(startPerson);
        return getMaxGain(startPerson, remainingPersons);
    }

    @Override
    public void executeDay() {
        executePuzzle(1, "Sample");
        executePuzzle(1, "Input");
        executePuzzle(2, "Sample");
        executePuzzle(2, "Input");
    }

    public static void main(String[] args) {
        new Aoc15Day13().execute(args);
    }
}
