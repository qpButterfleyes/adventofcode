package aoc.y2015.day07;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aoc.y2015.Aoc15Day;

public class Aoc15Day07 extends Aoc15Day {

    private static final Pattern LINE_PATTERN = Pattern
            .compile("((([a-z]+) |(\\d+) )?([A-Z]+) )?(([a-z]+)|(\\d+)) -> ([a-z]+)");

    private enum Gate {
        NONE, NOT, AND, OR, LSHIFT, RSHIFT;

        static Gate getByName(String name) {
            for (Gate value : values()) {
                if (value.name().equals(name)) {
                    return value;
                }
            }

            return NONE;
        }
    }

    private static class Connection {

        String in1Name;

        Integer in1Value;

        String in2Name;

        Integer in2Value;

        Gate gate;

        String outName;

        Integer outValue;
    }

    private List<Connection> buildConnectionList(String resPath) {
        List<Connection> connectionList = new ArrayList<>();
        List<Matcher> matcherList = buildMatcherList(resPath, LINE_PATTERN);

        for (Matcher matcher : matcherList) {
            Connection connection = new Connection();
            connectionList.add(connection);
            connection.in1Name = matcher.group(3);
            connection.in1Value = matcher.group(4) != null ? Integer.valueOf(matcher.group(4)) : null;
            connection.gate = Gate.getByName(matcher.group(5));
            connection.in2Name = matcher.group(7);
            connection.in2Value = matcher.group(8) != null ? Integer.valueOf(matcher.group(8)) : null;
            connection.outName = matcher.group(9);
        }

        return connectionList;
    }

    private int computeSignal(List<Connection> connectionList) {
        while (true) {
            Iterator<Connection> it = connectionList.iterator();

            while (it.hasNext()) {
                Connection connection = it.next();

                if (((connection.gate == Gate.NONE) || (connection.gate == Gate.NOT) || (connection.in1Value != null))
                        && (connection.in2Value != null)) {
                    switch (connection.gate) {
                    case NOT:
                        connection.outValue = ~connection.in2Value & 0xFFFF;
                        break;
                    case AND:
                        connection.outValue = connection.in1Value & connection.in2Value;
                        break;
                    case OR:
                        connection.outValue = connection.in1Value | connection.in2Value;
                        break;
                    case LSHIFT:
                        connection.outValue = (connection.in1Value << connection.in2Value) & 0xFFFF;
                        break;
                    case RSHIFT:
                        connection.outValue = connection.in1Value >>> connection.in2Value;
                        break;
                    default:
                        connection.outValue = connection.in2Value;
                    }

                    if ("a".equals(connection.outName)) {
                        return connection.outValue;
                    }

                    it.remove();

                    for (Connection conn : connectionList) {
                        if (connection.outName.equals(conn.in1Name)) {
                            conn.in1Value = connection.outValue;
                        }

                        if (connection.outName.equals(conn.in2Name)) {
                            conn.in2Value = connection.outValue;
                        }
                    }
                }
            }
        }
    }

    private void overrideInputSignal(List<Connection> connectionList, int signal) {
        for (Connection connection : connectionList) {
            if ("b".equals(connection.outName)) {
                connection.in2Value = signal;
            }
        }
    }

    @Override
    public long getPuzzle1Long(String resPath) {
        List<Connection> connectionList = buildConnectionList(resPath);
        return computeSignal(connectionList);
    }

    @Override
    public long getPuzzle2Long(String resPath) {
        List<Connection> connectionList = buildConnectionList(resPath);
        int signal = computeSignal(connectionList);
        connectionList = buildConnectionList(resPath);
        overrideInputSignal(connectionList, signal);
        return computeSignal(connectionList);
    }

    public static void main(String[] args) {
        new Aoc15Day07().execute(args);
    }
}
